function [x,y]=ginputRect(fig,ax)
    
    if (nargin==0)
        fig=gcf;
        ax=gca;
    end
     
    
    figure(fig)
    set(ax,'Units','normalized'); 
    axesposition=get(ax,'Position');
    
    posOld=get(ax,'CurrentPoint');
    xf1=posOld(1,1);
    yf1=posOld(1,2);
    
   for (i=1:4)
        hlines(i)=annotation('line',[0 0],[0 0]);
        set(hlines(i),'Color',[0.5 0.5 0.5]);
   end
   
    vars=struct('hlines',hlines);
    set(ax,'UserData',vars);
    set(fig,'UserData',ax);
    
    set(fig,'WindowButtonMotionFcn',@moveMouseFn);
    set(fig,'WindowButtonDownFcn',@FinMouseFn);
    
    waitfor(fig,'WindowButtonDownFcn');
    pos=get(ax,'CurrentPoint');    
   
    if (posOld(1,1)==pos(1,1) || posOld(1,2)==pos(1,2))
        x=[1 1]; y=[1 1];
       % return 
    end    
    
    
    %pos=get(ax,'CurrentPoint');
    xf1=pos(1,1);
    yf1=pos(1,2);

    %[xf1,yf1]=ginput(1);
      % if (xf1<0 || xf1 >1 || yf1<0 || yf1 >1 )
      %   x=[0 0]; y=[0 0];
      %   return
    % end
    figcoord=dsxy2figxy(ax,[xf1 yf1 0 0]);
%    axesposition
    if (figcoord(1)<axesposition(1) ||figcoord(1)>axesposition(1)+axesposition(3) || figcoord(2)<axesposition(2) ||figcoord(2)>axesposition(2)+axesposition(4) )
       delete(vars.hlines);
        x=[1 1]; y=[1 1];
       return 
    end
    hrect=annotation('rectangle',dsxy2figxy(ax,[xf1 yf1 0 0]));
    vars=struct('hlines',hlines,'rect',hrect,'x1',xf1,'y1',yf1);
    set(ax,'UserData',vars);
    set(fig,'WindowButtonMotionFcn',@moveMouseFn);
    set(fig,'WindowButtonDownFcn',@FinMouseFn);
    waitfor(fig,'WindowButtonDownFcn');
    vars=get(ax,'UserData');
    delete(vars.hlines);
    pos1=get(ax,'CurrentPoint');
    x=[vars.x1 pos1(1,1)];
    y=[vars.y1 pos1(1,2)];
        
end

function moveMouseFn(fh,e)
ax=get(fh,'UserData');    
pos=get(ax,'CurrentPoint');    
axesposition=get(ax,'Position');
vars=get(ax,'UserData');
    if (isfield(vars,'hlines'))
       figcoord=dsxy2figxy(ax,[ pos(1,1) pos(1,2) 0 0]);
       if (figcoord(1)>axesposition(1) && figcoord(1)<axesposition(1)+axesposition(3) && figcoord(2)>axesposition(2) && figcoord(2)<axesposition(2)+axesposition(4) )
        p=figcoord;
        for (i=1:4)
            if (i>2)
                espilon=1;
            else
                espilon=-1;
            end
            set(vars.hlines(i),'Position',[ p(1)+0.02*espilon*rem(i,2) p(2)+0.02*espilon*rem(i+1,2) espilon*rem(i,2) espilon*rem(i+1,2)]);
        end
       end
    end
    if (isfield(vars,'rect')) 
        set(vars.rect,'Position',dsxy2figxy(ax,[ vars.x1 vars.y1 (pos(1,1)-vars.x1) (pos(1,2)-vars.y1)]));
    end
end

function FinMouseFn(fh,e)
    set(fh,'WindowButtonMotionFcn','');
    set(fh,'WindowButtonDownFcn','');
end