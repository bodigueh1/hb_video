function varargout = hb_calcBkg(varargin)
% HB_CALCBKG M-file for hb_calcBkg.fig
%      HB_CALCBKG, by itself, creates a new HB_CALCBKG or raises the existing
%      singleton*.
%
%      H = HB_CALCBKG returns the handle to a new HB_CALCBKG or the handle to
%      the existing singleton*.
%
%      HB_CALCBKG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HB_CALCBKG.M with the given input arguments.
%
%      HB_CALCBKG('Property','Value',...) creates a new HB_CALCBKG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before hb_calcBkg_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to hb_calcBkg_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help hb_calcBkg

% Last Modified by GUIDE v2.5 24-Feb-2010 11:14:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hb_calcBkg_OpeningFcn, ...
                   'gui_OutputFcn',  @hb_calcBkg_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before hb_calcBkg is made visible.
function hb_calcBkg_OpeningFcn(hObject, eventdata, handles, varargin)
%clf
set(handles.saveAndExit,'UserData',0)
assignin('base','h0',handles);
handles.video=varargin{1};
set(handles.figure1,'CurrentAxes',handles.axes1);
handles.video.show();
%set(handles.axes1,'DataAspectRatioMode','manual')
%set(handles.axes1,'DataAspectRatio',[1 1 1])
set(handles.axes1,'Visible','off');
colormap(gray);
set(handles.finishDraw,'UserData',0);
set(handles.DrawPoly,'UserData',0);
set(handles.finishDraw,'Enable','off');
handles.allrect=[];
handles.roiBk=logical(0.*handles.video.currentIm);
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = hb_calcBkg_OutputFcn(hObject, eventdata, handles) 
waitfor(handles.saveAndExit,'UserData');
if (ishandle(handles.saveAndExit))
    varargout{1} =get(handles.saveAndExit,'UserData');
else
     varargout{1} =[];
end
close


% --- Executes on button press in DrawPoly.
function DrawPoly_Callback(hObject, eventdata, handles)
if (get(hObject,'UserData')==0)
    set(hObject,'UserData',1);
    set(hObject,'Enable','off');
    set(handles.finishDraw,'Enable','on');
    set(handles.finishDraw,'UserData',0);
    set(handles.DrawPoly,'Enable','off');
    set(handles.calculate,'Enable','off');
    set(handles.saveAndExit,'Enable','off');

    while  (get(handles.finishDraw,'UserData')==0)
        set(handles.figure1,'CurrentAxes',handles.axes1);
        
        [xi,yi]=ginputRect(handles.figure1,handles.axes1);
        % on recup le handle
        if (xi(1)==1 && xi(2)==1 && yi(1)==1 && yi(2)==1 )
         set(handles.figure1,'WindowButtonDownFcn','');
          else
            ff=get(handles.axes1,'UserData');
            handles.allrect=[handles.allrect;ff.rect];
            [m,n]=size(handles.roiBk);
            x=ones(m,1)*[1:n];
            y=[1:m]'*ones(1,n);
            handles.roiBk=(handles.roiBk | ( x >= min(xi(1:2)) & x <= max(xi(1:2)) & y>=min(yi(1:2)) & y <= max(yi(1:2)) ) );
            %set(handles.calculate,'Enable','on');
            set(handles.figure1,'CurrentAxes',handles.axes2)
            imagesc(handles.roiBk);
            colormap(gray);
            set(handles.axes2,'DataAspectRatioMode','manual')
            set(handles.axes2,'DataAspectRatio',[1 1 1])
            set(handles.axes2,'Visible','off');
             set(handles.figure1,'currentAxes',handles.axes1)
        end
        guidata(hObject, handles);
    end
    set(hObject,'UserData',0);
end

% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
handles.roiBk=logical(0.*handles.video.currentIm);
delete(handles.allrect);
handles.allrect=[];
set(handles.finishDraw,'UserData',0);
set(handles.DrawPoly,'UserData',0);
set(handles.finishDraw,'Enable','off');
set(handles.calculate,'Enable','off');
set(handles.saveAndExit,'Enable','off');
set(handles.DrawPoly,'Enable','on');
guidata(hObject, handles);


function PolyOrder_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function PolyOrder_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in calculate.
function calculate_Callback(hObject, eventdata, handles)


set(handles.DrawPoly,'Enable','off');
set(handles.calculate,'Enable','off');
set(handles.saveAndExit,'Enable','off');
set(handles.reset,'Enable','off');
drawnow;
n=str2num(get(handles.PolyOrder,'String'));
[p,handles.imfit]=planefit(handles.video.currentIm,n,handles.roiBk); 
set(handles.figure1,'currentAxes',handles.axes2)
imagesc(handles.imfit);
colormap(gray);
set(handles.axes2,'DataAspectRatioMode','manual')
set(handles.axes2,'DataAspectRatio',[1 1 1])
set(handles.axes2,'Visible','off');
set(handles.figure1,'currentAxes',handles.axes3)
imagesc(handles.video.currentIm./handles.imfit);
colormap(gray);
set(handles.axes3,'DataAspectRatioMode','manual')
set(handles.axes3,'DataAspectRatio',[1 1 1])
set(handles.axes3,'Visible','off');

set(handles.DrawPoly,'Enable','on');
set(handles.reset,'Enable','on');
set(handles.saveAndExit,'Enable','on');
set(handles.calculate,'Enable','on');
guidata(hObject, handles);



% --- Executes on button press in saveAndExit.
function saveAndExit_Callback(hObject, eventdata, handles)
% hObject    handle to saveAndExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (isfield(handles,'imfit'))
    handles.output=handles.imfit;
    set(hObject,'UserData',handles.imfit);
else
    set(hObject,'UserData',[]);
end
guidata(hObject, handles);


% --- Executes on button press in finishDraw.
function finishDraw_Callback(hObject, eventdata, handles)
% hObject    handle to finishDraw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1,'WindowButtonDownFcn','');
set(handles.figure1,'WindowButtonMotionFcn','');
set(handles.finishDraw,'UserData',1);
set(handles.DrawPoly,'Enable','on');
set(handles.finishDraw,'Enable','off');
if (sum(sum(handles.roiBk))>10)
    set(handles.calculate,'Enable','on');
end

