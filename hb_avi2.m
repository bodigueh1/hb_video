classdef hb_avi2
%HB_AVI2   
% THIS IS VERSION 2.02  PLEASE REPORTS BUGS TO THE AUTHOR
% Class permettant de lire des videos ou des s�quences d'images et d'y
% faire des op�rations basiques : 
%- background
%- r�gion d'in�teret
%- seuillage
%
%Cette classe peut �tre utilis�e en utilisant le "gui" hb_video 
% 
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%
% LIST OF PRINCIPAL METHODS
%  hb_avi() : constructor
%  read() : lit une image et la transforme en fonction des options de background
%  show() : affiche l'image courante
%  setBackground : utilise l'image courante comme background. 
%  thresholding() : r�alise une binarisation de l'image
%  setThreshold() : d�finit les seuils de binarisation
%  makeFilm(im_start,im_end,opt) : fait un film 
%  changeLocation() : modifie le chemin vers le fichier

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LIST OF REQUIRED FILES
% 
% hb_get_files.m
% hb_fileparts.m
%
% pour le gui :
% hb_gui_video.m
% hb_gui_video.fig
% 
% pour le threshold :
% hb_thresh_gui.m
% hb_thresh_gui.fig


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%VERSION LOG
% 1.00 => 1.01 : possibilit� de faire un filtre sur l'image binaris�e. (update de hb_thresh_gui)
% Possibilit� de travailler sur des s�quences d'images num�rot�s (update de
% hb_fileparts) 
% 1.01 => 1.02 : rajout de la fonction makeFilm()
% 1.02 => 1.03 : rajout de la possibilit� de faire du imresize
% 1.03 => 1.04 : rajout des r�glages d'affichage
% 1.04 => 1.05 : rajout de la rotation des images (� affectuer apres background et black object)
% 1.05 => 1.06 : rajout de la fonction changeLocation
% 1.06 => 2.01 : refonte totale de la classe 
% 2.01 => 2.02 : passage � MMREADER au lieu de aviread
% 11/05 : rajout d'une transformation affine
% 2/07/2013 : ajout de d�tection de particules et bpass
% 01/2016 : ajout de la possibilit� d'un background eclairage
% 02/2016 : ajout d'un pre-filter sur le threshold
% 02/2016 : correction de l'�criture video pour version 2013 et plus
%%%%%


   properties
       path; % contains path to the selected video/image seq
       filename; % contains the filename of selected video/image seq
       fullpath;  % contains the fullpath of selected video/image seq
       aviInfo; % contains some informations about the video/image seq
                % - NumFrames principalement
       
       time;  % contains the time serie of the video or of the image seq
       imageType;  %avi or images dependending of the type 'none' means that nothnig is loaded 
       scale; % contains the scale of the image (m/px)
       
       imageTimeZero=1; % nb image of time 0
       
       currentIm;  % contains the current image. It is initialize after every call to the read() method
       currentNbIm=1; % contains le number of the current image. It is initialize after every call to the read() method
       imBrut;  %contains the current image without any applied transformation. It is initialize after every call to the () method

       currentImBin; % contains the current binary images. It is initialized after every call to the thresholding() method
       % Note this properties has the following fiels : 
       % mainLevel : l'image binaris�e
       % upperLevel : l'image binaris�e en utilisant un seuil haut (si option d�finie)
       % lowerLevel : l'image binaris�e en utilisant un seuil bas (si option d�finie)
       % 
       useDataThreshold=false; %0 ou 1 si on veut ou on utiliser les seuillages d�finies manuellement (seuillage auto avec 0)
       dataThreshold; % contains informations about the thresholding options :
       % mainLevel : le niveau de seuillage pr�d�f�fini
       % useInterval : 0 ou 1 pour utiliser les upper et lower level
       % minImage : valeur utilis�e pour la normalisation en cas de seuillage fixe
       % maxImage : valeur utilis�e pour la normalisation en cas de seuillage fixe
       % postFilter : si sup�rieur � 0 filtrage de l'image binaire avec un filtre de taille postFilter
       % imresizeScale=1; 
       
       dataAff; % contains informations about the user display
     
       background;
       %
       % - useBackground : 0 pas de background
       %                   1  normalisation
       %                   2  soustraction
       %                   3  soustraction + normalisation
       % - imBack : l'image de background
       
       ROI;  % contains the region-of-interest 
       
       postOperations;  
       %  .background
       %                   0 pas de background
       %                   1  normalisation
       %                   2  soustraction
       %                   3  soustraction + normalisation
       %  .ROI
       %  .crop
       %  .rotate
       %  .resample
       %  .threshold
       %  .negatif
       % .afineTranform
       % .colors
       
       postOperationsInfos;
       % .colors
       
       specialInfos; 
       % this field enables the user to store any informations about the
       % video
       comment;
       %this filed enables the user to store informations
       
   end

   methods
       function obj=hb_avi2(fullpath)
          if nargin == 0
              obj=obj.changeLocation();
          else
              obj=obj.changeLocation(fullpath);
          end
          
          obj.dataAff.auto=1;
          obj.background.useBackground=0;
          obj.background.useBackgroundRemovedObject=false;
          obj.currentImBin.mainLevel=false;
          obj.useDataThreshold=false;

          obj.postOperations=obj.initPostOperations();
          
          if (strcmp(obj.imageType,'none')~=1)
              obj.ROI=ones(obj.aviInfo.Height,obj.aviInfo.Width)>0;
          end
       end
       
       function postOperations=initPostOperations(obj)
                postOperations.backgroud=0;
                postOperations.background=0;
                postOperations.ROI=0;
                postOperations.crop=0;
                postOperations.rotate=0;
                postOperations.resample=0;
                postOperations.threshold=0;
                postOperations.negatif=0;
                postOperations.flipUD=0;
                postOperations.flipLR=0;
                postOperations.affineTransform=0;
                postOperations.bpass=0;
                postOperations.pkfnd=0;
                postOperations.trace=0;
                postOperations.userdefined=0;
                postOperations.filter=0;
                postOperations.pkfnd=0;
                postOperations.transpose=0;
                
       end
           
           
       %%LECTURE D'UNE IMAGE%%
       function obj=read(obj, num, postOperations)           
          if (nargin==3)
            oldOperations=obj.postOperations;
            obj.postOperations=postOperations;
          end
          im=[];
          switch(obj.imageType)
              case 'images'
                    im.cdata=imread(fullfile(obj.path,obj.aviInfo.files(num).name));
              case 'stack'
                    im.cdata=imread(obj.fullpath,'index', num,'info',obj.aviInfo.iminfo );
              case 'table'
                    im.cdata=obj.aviInfo.data(:,:,num);
              case 'avi'
                 v=datevec(version('-date'));
                    if (v(1)>=2014)
                      im.cdata=obj.specialInfos.mmobj.read(num);
                    else                        
                im=aviread(obj.fullpath,num);
                    end
              %  if (~isfield(obj.specialInfos,'mmobj')) 
              %      obj.specialInfos.mmobj=mmreader(obj.fullpath);
              %  end
%                im.cdata=read(obj.specialInfos.mmobj,num);
              case 'matlabImages'
                im.cdata=importdata(fullfile(obj.path,obj.aviInfo.files(num).name));
              case 'imagesRaw'
                im.cdata=imraw2(fullfile(obj.path,obj.aviInfo.files(num).name));
              case 'lsm'
                im=tiffread(obj.fullpath,num*2-1);
                %im.cdata=im.(obj.aviInfo.channSelected);
                im.cdata=im.data;
              case 'nd2'
                if (isempty(obj.specialInfos.reader))
                     obj.specialInfos.reader=bfGetReader(obj.fullpath);
                     obj.specialInfos.omeMeta = obj.specialInfos.reader.getMetadataStore();
                end
                  im.cdata=bfGetPlane(obj.specialInfos.reader,num);
          end
          if (isempty(im))
              errordlg('Unable to read file');
          end
          
          if (ndims(im.cdata)==3)
               if (isfield(obj.postOperationsInfos,'colors'))
                   Image=(obj.postOperationsInfos.colors(1)*double(im.cdata(:,:,1))+obj.postOperationsInfos.colors(2)*double(im.cdata(:,:,2))+obj.postOperationsInfos.colors(3)*double(im.cdata(:,:,3)))/sum(obj.postOperationsInfos.colors);
               else               
                  Image = (double(rgb2gray(im.cdata)));
               end
          else
            Image = double(im.cdata);
          end
          if (max(max(Image))>2^15) 
             %%REPLIEMENT 16bits
             %Image(Image>=2^15)=Image(Image>=2^15)-2^16;
             %Image=2^15+Image;
          end

          obj.imBrut=im.cdata;

          %debut PostOperation         
          %BACKGROUND
          if (obj.postOperations.background>0)
              if (obj.background.useBackground==2)
                 Image=Image-obj.background.imBack;
              elseif (obj.background.useBackground==1)
                Image=Image./(obj.background.imBack);
              elseif (obj.background.useBackground==3)
                Image=(Image-obj.background.imBack)./(obj.background.imBack+1);
              elseif (obj.background.useBackground==4)
                 Image=(Image-obj.background.imBack)./(obj.background.eclairage);
              end
          end

          %NEGATIF
          if (obj.postOperations.negatif>0)
            Image=-1.*Image;
          end
          
         
          %ROI
          if (obj.postOperations.ROI>0)
               Image=Image.*obj.ROI;
               ROI=(obj.ROI & isfinite(Image));
          else
               ROI=isfinite(Image);
          end
          
          %ROTATION
          if (obj.postOperations.rotate>0 && isfield(obj.postOperationsInfos,'rotationAngle') && obj.postOperationsInfos.rotationAngle~=0)
            Image=imrotate(Image,obj.postOperationsInfos.rotationAngle,'bilinear','crop');
          end

          %CROP
              if ( obj.postOperations.crop>0)
              if (isfield(obj.postOperationsInfos,'crop'))
                  x=[obj.postOperationsInfos.crop.lx:obj.postOperationsInfos.crop.rx];
                  y=[obj.postOperationsInfos.crop.ly:obj.postOperationsInfos.crop.ry];
                  Image=Image(y,x);
              end
          end
          
          %RESAMPLE
          if ( obj.postOperations.resample>0)
              if (isfield(obj.postOperationsInfos,'resample') && obj.postOperationsInfos.resample~=1)
                    Image = imresize(Image, obj.postOperationsInfos.resample);
              end
          end
         
          obj.currentNbIm=num;   
          

          
          %AFFINETRANSFORM
          if (obj.postOperations.affineTransform>0)
    
              Image=imageAffineTransform(Image,obj.postOperationsInfos.affineTransform);
          end
        
          
          % FLIPS
          if (obj.postOperations.flipLR>0 )
               Image=fliplr(Image);
          end
          if (obj.postOperations.flipUD>0 )
              Image=flipud(Image);
          end
          if (isfield(obj.postOperations, 'transpose') && obj.postOperations.transpose>0 )
              Image=Image';
          end
          % BPASS
          if (obj.postOperations.bpass>0)
              Image=bpass(Image,obj.postOperationsInfos.bpass(1),obj.postOperationsInfos.bpass(2));
          end
         
          % FILTER
          if (isfield(obj.postOperations, 'filter') && obj.postOperations.filter>0)
              if strncmpi(obj.postOperationsInfos.filter.filterType,'disk',4)
                      h=fspecial('disk',obj.postOperationsInfos.filter.paramFilter(1));
              elseif strncmpi(obj.postOperationsInfos.filter.filterType,'gaus',4)
                      h=fspecial('gaussian',obj.postOperationsInfos.filter.paramFilter(1), obj.postOperationsInfos.filter.paramFilter(2));
              elseif strncmpi(obj.postOperationsInfos.filter.filterType,'aver',4)
                      h=fspecial('average',obj.postOperationsInfos.filter.paramFilter(1));
              end
              Image=imfilter(Image,h,'replicate'); % filtre avec l'option replicate
          end
          
          
        %USER
          if (isfield(obj.postOperations,'userdefined') && obj.postOperations.userdefined>0)
             if (nargin(obj.postOperationsInfos.userdefined)>=2)
                 Image=obj.postOperationsInfos.userdefined(Image,obj); 
             else
                 Image=obj.postOperationsInfos.userdefined(Image);
             end
          end
          
          
           % PEAK FIND
          if (obj.postOperations.pkfnd>0)
               try
                   obj.postOperationsInfos.pkfnd.results=pkfnd(Image,obj.postOperationsInfos.pkfnd.Imin,obj.postOperationsInfos.pkfnd.size);
               catch ME
                    disp(ME.message);
                    obj.postOperationsInfos.pkfnd.results=[];
               end
          end
          
          if (obj.postOperations.trace>0)
            if (~isempty(obj.currentIm) && size(obj.currentIm,1)==size(Image,1) && size(obj.currentIm,2)==size(Image,2))
                  Image=max(Image,obj.currentIm);
            end
          end
          
          %BIDOUILLE
       %   Image=conv2(Image,[1 1 1 1 1; 1 1 1 1  1; 1 1 5 1 1; 1 1 1 1  1; 1 1 1 1  1]/29,'same');
          
          
          obj.currentIm=Image;
          %BINARISATION
          if ( obj.postOperations.threshold>0)
                obj=obj.thresholding();
                obj.currentIm=obj.currentImBin.mainLevel; 
          end

          if (nargin>=3)
            obj.postOperations=oldOperations;
          end
       end
       
       
      
       
       
       %%AFFICHAGE DE L'IMAGE COURANTE%%
       function obj=show(obj,ax,imageHandle)
           if nargin==1
               ax=gca;
           end
           if (~isfield(obj.dataAff,'auto'))
                obj.dataAff.auto=1;
           end
           if (obj.dataAff.auto==1)
                imagesc(obj.currentIm)
                obj.dataAff.limits=[min(min(obj.currentIm)) max(max(obj.currentIm))];
           else
                if (~isfield(obj.dataAff,'limits'))
                    obj.dataAff.limits=[min(min(obj.currentIm)) max(max(obj.currentIm))];
                end
                imagesc(obj.currentIm,obj.dataAff.limits)
                
           end
           if (isfield(obj.dataAff,'gamma') && obj.dataAff.gamma~=1 )
                    obj.dataAff.colmap=(ones(3,1)*(linspace(0,1,64).^obj.dataAff.gamma))';
                    set(gcf,'colormap',obj.dataAff.colmap);
           else
                colormap(gray)
           end
       end
        
       %%DEFINIT UN BACKGROUND + BLACK OBJECT AUTO%% mettre false pour pas
       %%de black object
       function obj=setBackground(obj,removeObj)
          if nargin == 1
            removeObj=false;
          end
         

          if (obj.currentNbIm==0 || 1==1 )%max(obj.postOperations)>0 )
              postOperations=obj.initPostOperations();
              obj=obj.read(obj.currentNbIm,postOperations);
          end
              if (isfield(obj.background,'filtering'))
                   if (obj.background.filtering)
                    h = fspecial('average',10);
                    obj.currentIm=imfilter(obj.currentIm,h,'replicate');
                   end
              end
              obj.background.useBackgroundRemovedObject=false;
              obj.background.imBack=obj.currentIm;
              if (removeObj)
                    obj.background.useBackgroundRemovedObject=true;
                    im=obj.background.imBack;
                    im=(im-min(min(im)))./max(max(im));
                    level=graythresh(im);
                    imBW=im2bw(im,level/2);
                    imBW2=imfill(~imBW,'holes');
                    obj.background.imBackRemovedObject=~imBW2;                    
              end
              if (obj.postOperations.background==0)
                  obj.background.useBackground=1;
                  obj.postOperations.background=1;
              end
          
       end
       
        %%DEFINIT UN BLACK OBJECT MANUEL%%
       function obj=setBlackObject(obj,imageBlackObject)
           obj.ROI=imageBlackObject; 
           obj=obj.read(obj.currentNbIm);
       end
       
       
       %%TRACE UN BLACK OBJET
       function obj=drawBlackObject(obj,mode)
           fh=figure();
           if (obj.postOperations.resample>0 || obj.postOperations.rotate || obj.postOperations.threshold) % need to reload
                travOp=obj.postOperations;
                travOp.resample=0;
                travOp.rotate=0;
                travOp.threshold=0;
                obj2=obj.read(obj.currentNbIm,travOp);
                obj2.show();
           else
               obj.show();    
           end
           colormap(gray);
           B=~roipoly;
           if (obj.postOperations.flipLR)
               B=fliplr(B);
           end
           if (obj.postOperations.flipUD)
               B=flipud(B);
           end    

           close(gcf);
           if (obj.postOperations.crop>0)
              y=obj.postOperationsInfos.crop.ly:obj.postOperationsInfos.crop.ry;
              x=obj.postOperationsInfos.crop.lx:obj.postOperationsInfos.crop.rx;
           else
              y=1:obj.aviInfo.Height;
              x=1:obj.aviInfo.Width;
           end
           switch(mode)
              case 'black'
                obj.ROI(y,x)=obj.ROI(y,x) & B;
              case 'white'
                obj.ROI(y,x)=obj.ROI(y,x) | ~B;
           end
       end
       
       
 
       function obj=deleteBlackObject(obj)
           obj.ROI=isfinite(obj.ROI);
       end

 
       %%SUPPRIME LE BACKGROUND %%
       function obj=clearBackground(obj)
              obj.postOperations.background=0;
              obj.background.useBackground=0;
              obj.background.useBackgroundRemovedObject=false;
              obj.background=rmfield(obj.background,'imBack');
              obj.background=rmfield(obj.background,'imBackRemovedObject');
       end
       
       %%LIT LA VIDEO EN ENTIER %%
       function obj=play(obj)
           if (strcmp(obj.imageType,'avi'))
               mov=aviread(obj.fullpath);
              movie(mov)
            else
               
          end  
       end
       
       % update CROP
 
       
       function ROI=getROI(obj)
            ROI=obj.ROI; 
            if (obj.postOperations.crop>0)
                ROI=ROI(obj.postOperationsInfos.crop.ly:obj.postOperationsInfos.crop.ry,obj.postOperationsInfos.crop.lx:obj.postOperationsInfos.crop.rx);
            end
            if (obj.postOperations.flipLR>0)
                ROI=fliplr(ROI);
            end
            if (obj.postOperations.flipUD>0)
                ROI=flipud(ROI);
            end
            if (obj.postOperations.transpose>0)
                ROI=ROI';
            end
            if (obj.postOperations.resample>0)
                ROI = imresize(ROI, obj.postOperationsInfos.resample);
            end
            if (obj.postOperations.rotate>0 && obj.postOperationsInfos.rotationAngle>0)
                ROI =imrotate(ROI,obj.postOperationsInfos.rotationAngle,'bilinear','crop');
            end
            if (obj.postOperations.affineTransform>0 )
                ROI =imrotate(ROI,obj.postOperationsInfos.rotationAngle,'bilinear','crop');
            end
       end           

       
       %%BINARISE L'IMAGE
       function obj=thresholding(obj)
            ROI=obj.getROI();
                  im=obj.currentIm;
                   if (isfield(obj.dataThreshold,'postFilter') && obj.dataThreshold.postFilter>0)
                    h = fspecial('disk',obj.dataThreshold.postFilter);
                    im=(imfilter(im,h,'replicate'));
                   end

            if (obj.useDataThreshold) 
                   obj.currentImBin.mainLevel=(ROI & im>obj.dataThreshold.mainLevel);
                
                  if (obj.dataThreshold.useInterval)
                      obj.currentImBin.upperLevel=(ROI & im>obj.dataThreshold.upperLevel);
                     obj.currentImBin.lowerLevel=(ROI & im>obj.dataThreshold.lowerLevel);                                              
                      
                  end
              else
                  travIm=im;
                  travIm(ROI)=travIm(ROI)-min(min(travIm(ROI)));
                  travIm(ROI)=travIm(ROI)./max(max(travIm(ROI)));
                  level=graythresh(travIm(ROI));
                  obj.currentImBin.mainLevel=(ROI & im2bw(travIm,level));
              end
              
       end
       
       %%REGLAGE DU SEUIL
       function obj=setThreshold(obj,value) 
           if (nargin==2)
            result.mainLevel=value;
            return
           end
           if (obj.postOperations.threshold) %% on doit reloader l'image
                postOp=obj.postOperations;
                postOp.threshold=0;
                obj=obj.read(obj.currentNbIm,postOp);
           end
           result=hb_thresh_gui(obj.currentIm,obj.getROI());
           if (~isempty(result))
               result.mainLevel=result.mainLevel*result.maxImage+result.minImage;
               result.lowerLevel=result.lowerLevel*result.maxImage+result.minImage;
               result.upperLevel=result.upperLevel*result.maxImage+result.minImage;
               obj.dataThreshold=result;
               obj.useDataThreshold=true;
           end
       end
       
       
       %%ECRIT UN FILM 
       function obj=makeFilm(obj, tab)
          
           v=datevec(version('-date'));
           if (v(1)>=2014)
               [Filename,PathName] = uiputfile('*'); 
               if (Filename==0)
                 return
               end
               answer = inputdlg('Frame per second','Video property',1,{'20'});
               S={'Motion JPEG AVI' 'Motion JPEG 2000' 'MPEG-4' 'Grayscale AVI' };
               [Selection,ok] = listdlg('PromptString',{'Chose video compression'},'InitialValue',1,'SelectionMode','single','ListString',S);
               v=VideoWriter(fullfile(PathName,Filename),S{Selection});
               v.FrameRate=str2num(answer{1});
               if  Selection==1 || Selection==3
                   answer = inputdlg('Quality:  From 1 (low quality) to 100 (high quality)','Video property',1,{'75'});
                   v.Quality = str2num(answer{1}); 
               end
               if Selection ==2 
                   answer = inputdlg('CompressionRatio:  Integer greater of equal to 1 (the higher the more compressed)','Video property',1,{'10'});
                    v.CompressionRatio= str2num(answer{1});         
                    v.MJ2BitDepth=8;
               end
                h=hb_waitbar();
                intM=256;
              
               %colormap
               if (isfield(obj.dataAff,'gamma') && obj.dataAff.gamma~=1 )
                  colmap=(ones(3,1)*(linspace(0,1,intM).^obj.dataAff.gamma))';
               else
                  colmap=(ones(3,1)*(linspace(0,1,intM)))';
%                  colmap=colormap(jet(intM));

               end
                open(v)
               for (i=tab)
                    obj=obj.read(i);
                    hb_waitbar(h,(i-tab(1))/length(tab));
                    if (Selection==1 || Selection==3)
                        if (obj.dataAff.auto==1)
                           F = im2frame(uint8((obj.currentIm-min(min(obj.currentIm)))/(max(max(obj.currentIm))-min(min(obj.currentIm)))*(intM-1)),colmap);
                        else
                           F = im2frame(uint8(max(0,min(intM-1,(obj.currentIm-obj.dataAff.limits(1))/(obj.dataAff.limits(2)-obj.dataAff.limits(1))*(intM-1)))),colmap);                       
                        end
                    else
                        % cas du jpeg 2000 ou du Grayscale
                         g=1;
                         if (isfield(obj.dataAff,'gamma') && obj.dataAff.gamma~=1 )
                             g=obj.dataAff.gamma;
                         end
                         if (obj.dataAff.auto==1)
                             F=uint8((obj.currentIm-min(min(obj.currentIm)))/(max(max(obj.currentIm))-min(min(obj.currentIm)))*(intM-1));
                         else
                             F=uint8(max(0,min(intM-1,((obj.currentIm-obj.dataAff.limits(1))/(obj.dataAff.limits(2)-obj.dataAff.limits(1))).^g*(intM-1))));
                         end
                        
                    end    
                    writeVideo(v,F);
               end
                close(v);
                close(h);
           else
          
           
               [Filename,PathName] = uiputfile('*.avi'); 
               if (Filename==0)
                   return
               end
               movT = avifile(fullfile(PathName,Filename),'compression','none');
               answer = inputdlg('Frame per second','Video property',1,{'5'});
               movT.fps = str2num(answer{1});          
               S={'IV50' 'MSVC' 'RLE' 'Cinepak' 'None' };
               [Selection,ok] = listdlg('PromptString',{'Chose video compression'},'InitialValue',5,'SelectionMode','single','ListString',S);
               if (ok==1)
                    movT.compression=S{Selection};
                    if (Selection ~=5 )
                        answer = inputdlg('Quality (1...100)','Video property',1,{'75'});
                        movT.quality = str2num(answer{1}); 
                    end
               else 
                   movT.compression='none';
               end
               h=waitbar(0,'Ecriture du film en cours ...');
               if (Selection == 1 )
                  intM=236;
               else
                  intM=256;
               end
               %colormap
               if (isfield(obj.dataAff,'gamma') && obj.dataAff.gamma~=1 )
                  colmap=(ones(3,1)*(linspace(0,1,intM).^obj.dataAff.gamma))';
               else
                  colmap=(ones(3,1)*(linspace(0,1,intM)))';
              
               end

               for (i=tab)
                    obj=obj.read(i);
                    waitbar((i-tab(1))/length(tab),h);
                    if (obj.dataAff.auto==1)
                       F = im2frame(uint8((obj.currentIm-min(min(obj.currentIm)))/(max(max(obj.currentIm))-min(min(obj.currentIm)))*(intM-1)),colmap);
                    else
                       F = im2frame(uint8(max(0,min(intM-1,(obj.currentIm-obj.dataAff.limits(1))/(obj.dataAff.limits(2)-obj.dataAff.limits(1))*(intM-1)))),colmap);                       
                    end
                    movT = addframe(movT,F);
               end
               close(h);
               movT=close(movT);
           end
       end
       
       function obj=makeImSeq(obj,tab)
            
           [Filename,PathName] = uiputfile();
             if (Filename==0)
               return
           end
           [P,Filename, ext]=fileparts(Filename);
           ext=lower(ext);
           if (isempty(ext))
               ext='tif';
           else
               ext=ext(2:length(ext));
           end
           h=waitbar(0,'Ecriture du film en cours ...');
           if (obj.postOperations.threshold>0)
               intM=2;
           else
               intM=256;
           end
           %colormap
           if (isfield(obj.dataAff,'gamma') && obj.dataAff.gamma~=1 )
              colmap=(ones(3,1)*(linspace(0,1,intM).^obj.dataAff.gamma))';
           else
              colmap=(ones(3,1)*(linspace(0,1,intM)))';
           end
           for (i=tab)
                obj=obj.read(i);
                waitbar((i-tab(1))/length(tab),h);
                if (obj.dataAff.auto==1)
                   F = (uint8((obj.currentIm-min(min(obj.currentIm)))/(max(max(obj.currentIm))-min(min(obj.currentIm)))*(intM-1)));
                else
                   F = (uint8(max(0,min(intM-1,(obj.currentIm-obj.dataAff.limits(1))/(obj.dataAff.limits(2)-obj.dataAff.limits(1))*(intM-1)))));                       
                end
                dateV=datestr(obj.time(i)/(24*3600),'yymmdd_HHMMSSFFF');
                imwrite(F,colmap,fullfile(PathName,[ Filename '_' dateV '.' ext]), ext);
           end
           close(h)
       end
       
       
       
       function obj=makeMatSeq(obj,tab)
            
           [Filename,PathName] = uiputfile();
             if (Filename==0)
               return
           end
           [P,Filename, ext]=fileparts(Filename);
           ext=lower(ext);
 
           h=waitbar(0,'Ecriture du film en cours ...');
           for (i=tab)
                obj=obj.read(i);
                waitbar((i-tab(1))/length(tab),h);
                dateV=datestr(obj.time(i)/(24*3600),'yymmdd_HHMMSSFFF');
                if (obj.postOperations.threshold==0) 
                    image=single(obj.currentIm);
                else
                    image=obj.currentIm;
                end
                save(fullfile(PathName,[ Filename '_' dateV '.mat']), 'image');
           end
           close(h)
       end
       
       
       %  methode permettant de modifier le chemin de la sequence d'image
       function obj=changeLocation(obj,fullpath)
          if nargin == 1
            [pp,filename]=hb_get_file();
            if (~ischar(filename))
              if (~ischar(obj.imageType))
                  obj.imageType='none';
              end
              return; 
            end
            obj.path=pp;
            obj.filename=filename;
            obj.fullpath=fullfile(pp,filename);
          else
              [pathstr, name, ext] = fileparts(fullpath);
              obj.path=pathstr;
              obj.filename=[name  ext];
              obj.fullpath=fullpath;
          end
          
          if (~ischar(obj.fullpath))
              if (~ischar(obj.imageType))
                  obj.imageType='none';
              end
              return; 
          end
          [pathstr,fiename, ext] = fileparts(obj.fullpath);
          if (~ischar(ext))
              obj.imageType='none';
              errordlg('Ce fichier est introuvable');
              return;
          end
          if (strcmpi(ext,'.avi')==1)
              obj.specialInfos.mmobj=VideoReader(obj.fullpath);
              obj.aviInfo.Height=obj.specialInfos.mmobj.Height;
              obj.aviInfo.Width=obj.specialInfos.mmobj.Width;
              obj.aviInfo.NumFrames=obj.specialInfos.mmobj.NumberOfFrames; 
              if (isempty(obj.aviInfo.NumFrames))
                 % read(obj.specialInfos.mmobj);
                 % obj.aviInfo.NumFrames=obj.specialInfos.mmobj.NumberOfFrames; 
                  fileinfo = aviinfo(obj.fullpath);
                  obj.aviInfo.NumFrames=fileinfo.NumFrames;
                  
              end
              obj.aviInfo.FramePerSecond=obj.specialInfos.mmobj.FrameRate; 
              obj.imageType='avi';
              obj.time=[0:1/obj.aviInfo.FramePerSecond:(obj.aviInfo.NumFrames-1)/obj.aviInfo.FramePerSecond];        
          elseif (strcmpi(ext,'.lsm')==1)
               obj.imageType='lsm';
               obj.specialInfos.lsminfos=lsminfo(obj.fullpath);
               obj.aviInfo.Height=obj.specialInfos.lsminfos.DimensionX;
               obj.aviInfo.Width=obj.specialInfos.lsminfos.DimensionY;
               obj.aviInfo.NumFrames=obj.specialInfos.lsminfos.ScanInfo.STACKS_COUNT*obj.specialInfos.lsminfos.ScanInfo.NUMBER_OF_PLANES;
               obj.time=obj.specialInfos.lsminfos.TimeStamps.Stamps - obj.specialInfos.lsminfos.TimeStamps.Stamps(1);
               
               obj.time=reshape(obj.time,length(obj.time),1)*ones(1,obj.specialInfos.lsminfos.ScanInfo.NUMBER_OF_PLANES);
               obj.time=obj.time';
               obj.time=obj.time(:);
               
               im=tiffread(obj.fullpath,1);
               if (isfield(im,'red'))
                     S={'red' 'green' 'blue' };
                    [Selection,ok] = listdlg('PromptString',{'Chose field'},'InitialValue',1,'SelectionMode','single','ListString',S);
                    obj.aviInfo.channSelected=S{Selection};
               else 
                   obj.aviInfo.channSelected='cdata';
               end
          elseif (strcmpi(ext,'.nd2')==1)
               obj.imageType='nd2';
               obj.specialInfos.reader=bfGetReader(obj.fullpath);
               obj.specialInfos.omeMeta = obj.specialInfos.reader.getMetadataStore();
            
               obj.aviInfo.Height=obj.specialInfos.omeMeta.getPixelsSizeY(0).getValue(); 
               obj.aviInfo.Width=obj.specialInfos.omeMeta.getPixelsSizeX(0).getValue(); 
               obj.aviInfo.NumFrames=obj.specialInfos.omeMeta.getPlaneCount(0);
               obj.aviInfo.PixelSizeX=double(obj.specialInfos.omeMeta.getPixelsPhysicalSizeX(0).value());
               obj.aviInfo.PixelSizeY=double(obj.specialInfos.omeMeta.getPixelsPhysicalSizeY(0).value());
               
               if obj.aviInfo.NumFrames>1
                   DeltaT=(double(obj.specialInfos.omeMeta.getPlaneDeltaT(0,obj.aviInfo.NumFrames-1).value) - double(obj.specialInfos.omeMeta.getPlaneDeltaT(0,0).value))/(obj.aviInfo.NumFrames-1);
                   obj.aviInfo.FramesPerSecond=1/DeltaT;
                end
              
               obj.time=1:obj.aviInfo.NumFrames;
               for i=1:length(obj.time)
                  obj.time(i)=double(obj.specialInfos.omeMeta.getPlaneDeltaT(0,i-1).value);
               end

               obj.aviInfo.Z=[];
               for i=1:length(obj.time)
                 obj.aviInfo.Z(i)=double(obj.specialInfos.omeMeta.getPlanePositionZ(0,i-1).value);
               end
          elseif (max(strcmpi(ext,{'.bmp','.png','.gif','.jpg','.tif','.tiff','.mat','.raw'}))==1)
              [pathstr,filename, ext, nom, day, tim] = hb_fileparts(obj.fullpath);
                files = dir(fullfile(obj.path,[nom '*' ext]));               

                if (strcmpi(ext,'.raw')==1 || (strcmpi(ext,'.mat')==1))
                    infos=struct('toto',1);

                else
                    infos=imfinfo(obj.fullpath);
                end
                obj.aviInfo.files=files;

                if (length(infos)>1)
                    obj.imageType='stack';
                    obj.aviInfo.NumFrames=length(infos);
                    obj.time=[1:obj.aviInfo.NumFrames];
                    obj.aviInfo.iminfo=infos;
            
                else
                    obj.imageType='images';
                    obj.aviInfo.NumFrames=length(obj.aviInfo.files);
                  
                    obj.time=[1:obj.aviInfo.NumFrames];
                    for (k=1:obj.aviInfo.NumFrames)
                        [pathstr,filename, ext, nom, day, tim] = hb_fileparts(obj.aviInfo.files(k).name);
                        obj.time(k)=tim;
                    end
                      [obj.time,perm]=sort(obj.time);
                    obj.aviInfo.files=files(perm);
                    
                    if ( strcmpi(ext,'.mat')==0)
                    try 
                        im=imread(obj.fullpath,1);
                    catch
                       if (strcmpi(ext,'.raw')==1)
                           im=imraw2(obj.fullpath);
                       else
                           im=imread(obj.fullpath);
                        end
                    end
                     [obj.aviInfo.Height,obj.aviInfo.Width,proff]=size(im);
                    end
                end
                
               obj.time=obj.time-obj.time(1);
               if length(obj.time)>1
                   obj.aviInfo.FramesPerSecond=mean(1./diff(obj.time));
               end
               if (strcmpi(ext,'.mat')==1)
                   obj.imageType='matlabImages';
                   im=importdata(obj.fullpath);
                   if (isnumeric(im) || islogical(im))
                       
                        if (size(im,3)>1)
                          obj.imageType='table';
                           obj.aviInfo.NumFrames=size(im,3);
                         obj.aviInfo.data=im;
                        end  
                            
                        obj.time=[1:obj.aviInfo.NumFrames];
                        obj.aviInfo.Height=size(im,1);
                        obj.aviInfo.Width= size(im,2);
                    
                    
                   else
                       obj.imageType='none';
                       errordlg('Ce type de fichier n''est pas pris en charge');
                       return;
                   end
               elseif (strcmpi(ext,'.raw')==1)
                   im=imraw2(obj.fullpath);
                   [obj.aviInfo.Height,obj.aviInfo.Width,proff]=size(im);
                   obj.imageType='imagesRaw'
               elseif (strcmpi(obj.imageType,'stack')==1)
                   im=imread(obj.fullpath,1);
                   [obj.aviInfo.Height,obj.aviInfo.Width,proff]=size(im);
               end
          else
              errordlg('Ce type de fichier n''est pas pris en charge')
              obj.imageType='none';
              return;
          end
       end
       
       %%verffication du path
       function obj=verifyPath(obj)
           d=dir(obj.fullpath);
           if (isempty(d))
               h=errordlg('The path cannot be found. Please chose a new path for the image sequence. ');
               uiwait(h)
               obj=obj.changeLocation();
           end
       end 
   end
end 
