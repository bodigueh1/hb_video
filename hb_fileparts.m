function [pathstr, filename, ext, nom,day,time]=hb_fileparts(filepath,frequence)

[pathstr, name, ext] = fileparts(filepath);

filename=name;
nom=name;
day=0;
time=0;
l=length(name);

%test si format dateheure
if (l>15 && all((isstrprop(name(l-14:l), 'digit')==[1 1 1 1  1 1 0 1 1 1 1 1 1 1 1])))
H=str2double(name(l-7:l-6));
MN=str2double(name(l-5:l-4));
S=str2double(name(l-3:l-2));
C=str2double(name(l-1:l));

D=str2double(name(l-14:l-13));
M=str2double(name(l-12:l-11));
Y=str2double(['20' name(l-10:l-9)]);

nom=name(1:l-15);

day = datenum(Y, M, D);
time=D*3600*24+H*3600+MN*60+S+C/100;

elseif (l>16 && all((isstrprop(name(l-15:l), 'digit')==[1 1 1 1  1 1 0 1 1 1 1 1 1 1 1 1])))

    
H=str2double(name(l-8:l-7));
MN=str2double(name(l-6:l-5));
S=str2double(name(l-4:l-3));
C=str2double(name(l-2:l));

D=str2double(name(l-11:l-10));
M=str2double(name(l-13:l-12));
Y=str2double(['20' name(l-15:l-14)]);

nom=name(1:l-16);

day = datenum(Y, M, D);
time=D*3600*24+H*3600+MN*60+S+C/1000;

    
else 
    % on cherche les digits
    a=regexp(name,'^(.*?)([0-9]+)$','tokens');
    if (~isempty(a))
    nom=a{1}{1};
%    if (l>4 && all((isstrprop(name(l-3:l), 'digit'))))
       
        time=str2double(a{1}{2});
        if (nargin>1)
            time=time/frequence;
        end
        day=0;
    else
        day=0;
        time=0;
    end
    
end
end
