function [PathName,FileName]=hb_get_file(filter)

if (nargin==0)
    filter='*.*';
end
global thePath
if (ischar(thePath))
    [FileName,PathName,FilterIndex] = uigetfile(fullfile(thePath,filter));
else
     [FileName,PathName,FilterIndex] = uigetfile(filter);
end
    thePath=PathName;

end