function varargout = hb_video214(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hb_video_OpeningFcn, ...
                   'gui_OutputFcn',  @hb_video_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
    
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before hb_video is made visible.
function hb_video_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
ff=fieldnames(handles);

guidata(hObject, handles);
if (nargin>=4)
   handles.video=varargin{1};
   handles.loaded=1;
   handles.video=handles.video.verifyPath();
   initAll(handles);
   reloadImage(hObject,handles);
end


function varargout = hb_video_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


% --- Executes on button press in select_file.
function select_file_Callback(hObject, eventdata, handles)
handles.loaded=0;
[pp,filename]=hb_get_file();
[pathstr, name, ext]=fileparts(fullfile(pp,filename));
if (strcmpi(ext,'.mat')==1) 
    a=whos('-file',fullfile(pp,filename));
    ind=find(strcmp({a.class},'hb_avi2'));
    if (length(ind)==1)
        S=load(fullfile(pp,filename),a(ind).name);
       handles.video=getfield(S,a(ind).name);
       handles.video=handles.video.verifyPath();
    elseif (length(ind)>1)
         ll={a(ind).name};
         a=a(ind);
         [selection, ok]=listdlg('ListString',ll,'SelectionMode','single','PromptString','choose one of the following variables');
         if (ok)
            S=load(fullfile(pp,filename),a(selection).name);
            handles.video=getfield(S,a(selection).name);
            handles.video=handles.video.verifyPath();
         end
    else
       handles.video=hb_avi2(fullfile(pp,filename));
    end
else
    handles.video=hb_avi2(fullfile(pp,filename));
end
if (strcmp(handles.video.imageType,'none')~=1)
    busyOn(handles,'on');
    handles.visible='on';
    handles.loaded=true;
    initAll(handles);
    guidata(hObject,handles);
    reloadImage(hObject,handles);
    busyOn(handles,'off');
end

% --- Executes on button press in loadFromWS.
function loadFromWS_Callback(hObject, eventdata, handles)
a=evalin('base','whos');
ind=strcmp({a.class},'hb_avi2');
if (max(ind)==1)
    if (sum(ind)==1)
       ok=1;
       nom=a(ind).name;
    else
        ll={a(ind).name};
        [selection, ok]=listdlg('ListString',ll,'SelectionMode','single','PromptString','choose one of the following variables');
        nom=ll{selection};
    end
    if (ok)
        handles.video=evalin('base',nom);
        handles.loaded=1;
        handles.video=handles.video.verifyPath();
        initAll(handles);
        guidata(hObject,handles);
        reloadImage(hObject,handles);
    end
else
    errordlg('Le workspace ne contient aucune variable de classe hb_avi2')
end

%Inititialise tout
function initAll(handles)

if (isfield(handles,'video'))
    
    set(handles.nb_total,'String',handles.video.aviInfo.NumFrames);
    %smart path display
        [p , f, e, nom]=hb_fileparts(handles.video.fullpath);
        sep1='';
        if (strcmpi(nom,f)==0)
            sep1='(...)';
        end
        fullname=[p filesep nom sep1 e];
        k=3;inds=find(p==filesep);
        while (length(fullname)>70 && k<=length(inds))
            thep=[p(1: inds(1)) '...' p(inds(k):length(p))];
            fullname=[thep filesep nom sep1 e];
            k=k+1;
        end
        set(handles.theFilePath,'String',fullname);
    
    if (isfield(handles.video.aviInfo,'FramesPerSecond'))
        set(handles.fps,'String',handles.video.aviInfo.FramesPerSecond);
    end

    set(handles.filename,'String',handles.video.filename);
    set(handles.end_video,'String',handles.video.aviInfo.NumFrames);
    set(handles.width,'String',handles.video.aviInfo.NumFrames);
    set(handles.backpopup,'Value',handles.video.postOperations.background+1);
    set(handles.negatif,'Value',handles.video.postOperations.negatif);
    
%    set(handles.dispImage,'enable','on');
%    set(handles.dispBrut,'enable','on');
    set(handles.openInNew,'enable','on');
    set(handles.record_video,'enable','on');
    set(handles.writeImSeq,'enable','on');
    set(handles.writeMatSeq,'enable','on');
    set(handles.background,'enable','on');
    set(handles.loadBkg,'enable','on');
     set(handles.loadLight,'enable','on');
    set(handles.calcBkg,'enable','on');
    set(handles.moinsmoins,'enable','on');
    set(handles.plusplus,'enable','on');
    set(handles.image_previous,'enable','on');
    set(handles.image_next,'enable','on');
    set(handles.play,'enable','on');
    set(handles.imageNum,'enable','on');
 %   set(handles.SetFinal,'enable','on');
    set(handles.LimitsAffInf,'enable','on');
    set(handles.LimitsAffSup,'enable','on');
    set(handles.LimitsAffMoinsSlide,'enable','on');
    set(handles.LimitsAffPlusSlide,'enable','on');
    set(handles.AutoLimits,'enable','on');
    set(handles.negatif,'enable','on');
    set(handles.save,'enable','on');
    set(handles.saveAs,'enable','on');
    set(handles.save2file,'enable','on');
    set(handles.threshold,'enable','on');
    set(handles.setInitial,'enable','on');
    set(handles.bpFilter,'enable','on');
    set(handles.bpassValueL,'enable','on');
    set(handles.bpassValueH,'enable','on');
    set(handles.pkfndImin,'enable','on');
    set(handles.pkfndSize,'enable','on');
    set(handles.pkfnd,'enable','on');
%    set(handles.sliderPkfndImax,'enable','on');
    set(handles.Trace,'enable','on');
    
    set(handles.threshold,'Value',handles.video.postOperations.threshold);
    if (isfield(handles.video.dataThreshold,'mainLevel'))
        set(handles.threshMenu,'enable','on'); 
    end
    if (handles.video.useDataThreshold)
        set(handles.threshMenu,'Value',2);
    else set(handles.threshMenu,'Value',1);
    end
    set(handles.showHisto,'enable','on');
%    set(handles.dispROI,'enable','on');
    set(handles.ROI,'enable','on','Value',handles.video.postOperations.ROI);
    set(handles.deleteROI,'enable','on');
    set(handles.invROI,'enable','on');
    set(handles.drawWhite,'enable','on');
    set(handles.drawBlack,'enable','on');
    set(handles.autoROI,'enable','on');
    set(handles.openInNew,'enable','on');
    set(handles.resampleFactor,'enable','on');
    set(handles.angleRotate,'enable','on');
    set(handles.rotate,'enable','on','Value',handles.video.postOperations.rotate);
    set(handles.flipUD,'enable','on','Value',handles.video.postOperations.flipUD);
    set(handles.flipLR,'enable','on','Value',handles.video.postOperations.flipLR);
    if ~isfield(handles.video.postOperations,'transpose')
        handles.video.postOperations.transpose=0;
    end

    set(handles.transpose,'enable','on','Value',handles.video.postOperations.transpose);
    
    set(handles.userDefined_check,'enable','on','Value',handles.video.postOperations.userdefined);
    set(handles.userDefined,'enable','on') ;
    if isfield(handles.video.postOperationsInfos,'userdefined')
        set(handles.userDefined,'Value',func2str(handles.video.postOperationsInfos.userdefined));
    end 
    
    set(handles.gamma0,'enable','on');
    set(handles.gammaSlider,'enable','on');
    if (isfield(handles.video.postOperationsInfos,'gamma'))
        set(handles.gamma0,'Value',handles.video.postOperationsInfos.gamma);
        set(handles.slider,'Value',log10(handles.video.postOperationsInfos.gamma));
    end
    
     set(handles.cropTLx,'enable','on');
     set(handles.cropTLy,'enable','on');
     set(handles.cropBRx,'enable','on');
     set(handles.cropBRy,'enable','on');
     set(handles.bottomRightButton,'enable','on');
     set(handles.topLeftButton,'enable','on');
     set(handles.ResetCrop,'enable','on');
%      if (isfield(handles.video.postOperations,'affineTransorm') )
%      	set(handles.affineTransorm,'enable','on','Value',handles.video.postOperations.affineTransorm);
%      else
%          set(handles.affineTransorm,'enable','on','Value',0);
%      end
%      if (isfield(handles.video.postOperationsInfos,'affineTransorm') )
%      	set(handles.vecteur_affineTransorm,'String',num2str(handles.video.postOperationsInfos.affineTransorm));
%      else
%          set(handles.affineTransorm,'String','0 0 0 0');
%      end
%      
         
    set(handles.Filter,'enable','on');
     %        set(handles.save2file,'enable','on');
%     
    if (isfield(handles.video.background,'imBack'))
        set(handles.backpopup,'enable','on');
        set(handles.backfilter,'enable','on');
%        set(handles.dispBkg,'enable','on');
    end
    
    if (handles.video.postOperations.crop>0)
        wi=handles.video.postOperationsInfos.crop.rx-handles.video.postOperationsInfos.crop.lx+1;
        he=handles.video.postOperationsInfos.crop.ry-handles.video.postOperationsInfos.crop.ly+1;
        set(handles.cropTLx,'String',num2str(handles.video.postOperationsInfos.crop.lx));
        set(handles.cropTLy,'String',num2str(handles.video.postOperationsInfos.crop.ly));
        set(handles.cropBRx,'String',num2str(handles.video.postOperationsInfos.crop.rx));
        set(handles.cropBRy,'String',num2str(handles.video.postOperationsInfos.crop.ry));
    else
        wi=handles.video.aviInfo.Width;
        he=handles.video.aviInfo.Height;
        set(handles.cropTLx,'String',num2str(1));
        set(handles.cropTLy,'String',num2str(1));
        set(handles.cropBRx,'String',num2str(wi));
        set(handles.cropBRy,'String',num2str(he));
    end
    
    if (handles.video.postOperations.resample>0)
        factor=handles.video.postOperationsInfos.resample;
        set(handles.resampleFactor,'String',num2str(factor));
        he=round(he*factor);
        wi=round(wi*factor);
    else
        set(handles.resampleFactor,'String','1');
    end
    if (handles.video.postOperations.rotate>0 && isfield(handles.video.postOperationsInfos,'rotationAngle'))    
        factor=handles.video.postOperationsInfos.rotationAngle;
        set(handles.angleRotate,'String',num2str(factor));
   else
        set(handles.angleRotate,'String','0');
    end
    
    if (isfield(handles.video.postOperations,'bpass') && handles.video.postOperations.bpass>0 && isfield(handles.video.postOperationsInfos,'bpass'))    
        set(handles.bpassValueL,'String',num2str(handles.video.postOperationsInfos.bpass(1)))
        set(handles.bpassValueH,'String',num2str(handles.video.postOperationsInfos.bpass(2)));
        set(handles.bpFilter,'Value',1);
    else
        set(handles.bpassValueL,'String','0.5');
        set(handles.bpassValueH,'String','5');
        set(handles.bpFilter,'Value',0);
    end
    
    if (isfield(handles.video.postOperations,'filter') && handles.video.postOperations.filter>0 )    
        set(handles.Filter,'value',1);
        if (strncmpi(handles.video.postOperationsInfos.filter.filterType,'dis',3))
             set(handles.FilterType,'value',1)
        elseif (strncmpi(handles.video.postOperationsInfos.filter.filterType,'gau',3))
             set(handles.FilterType,'value',2)
        elseif (strncmpi(handles.video.postOperationsInfos.filter.filterType,'ave',3))
             set(handles.FilterType,'value',3)
        end
        set(handles.ParamFilter1,'string',num2str(handles.video.postOperationsInfos.filter.paramFilter(1)));
        set(handles.ParamFilter2,'string',num2str(handles.video.postOperationsInfos.filter.paramFilter(2)));
 
        
        if get(handles.FilterType,'value')==2 
            set(handles.ParamFilter2,'enable','on');
        else
            set(handles.ParamFilter2,'enable','off');
        end
    else
         set(handles.Filter,'value',0)
         set(handles.ParamFilter1,'enable','off')
         set(handles.ParamFilter2,'enable','off')
         set(handles.FilterType,'enable','off')
    end

    
    if (isfield(handles.video.postOperations,'pkfnd') && handles.video.postOperations.pkfnd>0 && isfield(handles.video.postOperationsInfos,'pkfnd'))    
        set(handles.pkfnd,'Value',1);
        set(handles.pkfndImin,'String',num2str(handles.video.postOperationsInfos.pkfnd.Imin));
        set(handles.pkfndSize,'String',num2str(handles.video.postOperationsInfos.pkfnd.size));
    else
        set(handles.pkfnd,'Value',0);            
    end
    
    set(handles.width,'String',num2str(wi));
    set(handles.height,'String',num2str(he));
end

function video=reloadImage(hObject,handles,im_handle)

    if nargin<3
        im_handle=0;
    end
    busyOn(handles,'on')
    handles.video=handles.video.read(handles.video.currentNbIm);
    initDataAff(hObject,handles,im_handle);
    set(handles.timeImage,'String',num2str(handles.video.time(handles.video.currentNbIm)));
    set(handles.imageNum,'String',handles.video.currentNbIm);
    if (handles.video.postOperations.pkfnd>0)
        hold on;
        cnt=handles.video.postOperationsInfos.pkfnd.results;
        if (size(cnt,1)>=1), plot(cnt(:,1), cnt(:,2), 'bo', 'markersize', 8); end
        hold off;
    end
    busyOn(handles,'off')
    video=handles.video;
    

   
function initDataAff(hObject,handles,im_handle)
    if nargin<3 
        im_handle=0;
    end
    d=handles.video.dataAff;
    set(handles.AutoLimits,'value',d.auto);
    M=max(max(handles.video.currentIm));
    m=min(min(handles.video.currentIm));
    if (d.auto==1)
        handles.video.dataAff.limits=[m M];
        d=handles.video.dataAff;
    end
    set(handles.LimitsAffMoinsSlide,'value',max(0,min(1,(d.limits(1)-m)/(M-m))));
    set(handles.LimitsAffPlusSlide,'value',max(0,min(1,(d.limits(2)-m)/(M-m))));
    set(handles.LimitsAffInf,'String',num2str(d.limits(1)));
    set(handles.LimitsAffSup,'String',num2str(d.limits(2)));
    if (isfield(handles.video.dataAff,'gamma')) 
        set(handles.gamma0,'String',num2str(handles.video.dataAff.gamma)); 
        set(handles.gammaSlider,'Value',log10(handles.video.dataAff.gamma));
    end
    if (im_handle==0)
        handles.video=handles.video.show(handles.axes1);
        set(handles.axes1,'DataAspectRatio',[1 1 1],'Visible','off');
    else
        set(im_handle,'cdata',handles.video.currentIm);
    end
 %   set(handles.displayButtonGroup,'SelectedObject',handles.dispImage);
    guidata(hObject,handles);    

function busyOn(handles,vis)
    set(handles.busy, 'visible',vis)
    drawnow

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% VIDEO CONTROL  %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function image_previous_Callback(hObject, eventdata, handles)
if (handles.video.currentNbIm>1)
    handles.video.currentNbIm=handles.video.currentNbIm-1;  
    reloadImage(hObject,handles);
end

function image_next_Callback(hObject, eventdata, handles)
if (handles.video.aviInfo.NumFrames>handles.video.currentNbIm)
    handles.video.currentNbIm=handles.video.currentNbIm+1;  
    reloadImage(hObject,handles);
end

function plusplus_Callback(hObject, eventdata, handles)

if (handles.video.aviInfo.NumFrames>handles.video.currentNbIm)
    pas=max(round(handles.video.aviInfo.NumFrames/20),10);
    handles.video.currentNbIm=min(handles.video.currentNbIm+pas,handles.video.aviInfo.NumFrames);  
    reloadImage(hObject,handles);
end

function moinsmoins_Callback(hObject, eventdata, handles)
if (handles.video.currentNbIm>1)
    pas=max(round(handles.video.aviInfo.NumFrames/20),10);
    handles.video.currentNbIm=max(handles.video.currentNbIm-pas,1); 
    reloadImage(hObject,handles);
end

function imageNum_Callback(hObject, eventdata, handles)
handles.video.currentNbIm=max(1,min(handles.video.aviInfo.NumFrames,round(str2double(get(hObject,'String')))));
reloadImage(hObject,handles);

function imageNum_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function play_Callback(hObject, eventdata, handles)
handles.playControl=1;
set(handles.stopPlaying,'enable','on');
set(handles.play,'enable','off');
set(handles.Trace,'enable','off');
%guidata(hObject,handles)
im_handle= findobj(handles.axes1,'Type','image');
i=get(handles.readFPS,'Value');
fps=[1000 20 10 5 2 1 0.5];
t=timer('ExecutionMode','fixedRate','timerFcn',{@playMovie,hObject,im_handle},'period',1/fps(i));
set(hObject,'userdata',t);
guidata(hObject,handles)         
start(t);

function playMovie(obj,event,hObject,im_handle)
handles=guidata(hObject);   
    if (handles.video.aviInfo.NumFrames>handles.video.currentNbIm)
         handles.video.currentNbIm=handles.video.currentNbIm+1;
         handles.video=reloadImage(hObject, handles,im_handle);
         drawnow
        guidata(hObject,handles);
    else
        stopPlaying_Callback(hObject,event,handles);
    end

    
function stopPlaying_Callback(hObject, eventdata, handles)
handles.playControl=0;
set(handles.stopPlaying,'enable','off');
set(handles.play,'enable','on');
set(handles.Trace,'enable','on');
t=get(handles.play,'userdata');
stop(t);
delete(t);  
guidata(hObject,handles)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DISPLAY PROPERTIES %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function displayButtonGroup_SelectionChangeFcn(hObject, eventdata,handles)
switch get(hObject,'Tag')   % Get Tag of selected object
    case 'dispImage'
        initDataAff(hObject,handles);
    case 'dispROI'
            imagesc(handles.video.getROI());
            set(handles.axes1,'DataAspectRatio',[1 1 1],'Visible','off');            
    case 'dispBkg'
        if (isfield(handles.video.background,'imBack')) 
            imagesc(handles.video.background.imBack);
        else
            
            imagesc([1 1 ; 1 1]);
        end
        set(handles.axes1,'DataAspectRatio',[1 1 1],'Visible','off');
    case 'dispBrut'
        imshow(handles.video.imBrut);
end


function dispPopup_Callback(hObject,eventdata,handles)
switch get(hObject,'Value')
    case 1
        initDataAff(hObject,handles);
    case 3
            imagesc(handles.video.getROI());
            set(handles.axes1,'DataAspectRatio',[1 1 1],'Visible','off');            
    case 2
        if (isfield(handles.video.background,'imBack')) 
            imagesc(handles.video.background.imBack);
        else
            
            imagesc([1 1 ; 1 1]);
        end
        set(handles.axes1,'DataAspectRatio',[1 1 1],'Visible','off');
    case 4
        imshow(handles.video.imBrut);
end
    




% --- Executes on button press in openInNew.
function openInNew_Callback(hObject, eventdata, handles)
figure()
get(handles.dispPopup,'Value')
switch get(handles.dispPopup,'Value')
   case 1
        handles.video.show();
        colormap(gray);
   case 2
       if (isfield(handles.video.background,'imBack'))
            imagesc(handles.video.background.imBack);
       else
           imagesc([1 1 ; 1 1]);
       end
       colormap(gray);
   case 3
        imagesc(handles.video.getROI());
        colormap(gray);
   case 4
        imshow(handles.video.imBrut);
end

function negatif_Callback(hObject, eventdata, handles)
handles.video.postOperations.negatif=1-handles.video.postOperations.negatif;
reloadImage(hObject,handles);

function LimitsAffPlusSlide_Callback(hObject, eventdata, handles)
 M=max(max(handles.video.currentIm));
     m=min(min(handles.video.currentIm));
     v=get(hObject,'value');
     handles.video.dataAff.limits(2)=m+(v)*(M-m);
initDataAff(hObject,handles)
    
function LimitsAffPlusSlide_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function LimitsAffMoinsSlide_Callback(hObject, eventdata, handles)
     M=max(max(handles.video.currentIm));
     m=min(min(handles.video.currentIm));
     v=get(hObject,'value');
     handles.video.dataAff.limits(1)=m+(v)*(M-m);
initDataAff(hObject,handles)
    
function LimitsAffMoinsSlide_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function LimitsAffSup_Callback(hObject, eventdata, handles)
handles.video.dataAff.limits=[ handles.video.dataAff.limits(1) str2double(get(hObject,'String')) ];
handles.video.dataAff.auto=0;
initDataAff(hObject,handles)

function LimitsAffSup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function LimitsAffInf_Callback(hObject, eventdata, handles)
handles.video.dataAff.limits=[ str2double(get(hObject,'String')) handles.video.dataAff.limits(2)];
handles.video.dataAff.auto=0;
initDataAff(hObject,handles)


function LimitsAffInf_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function AutoLimits_Callback(hObject, eventdata, handles)
a=get(hObject,'Value');
if (a==0)
    handles.video.dataAff.auto=0;
else
    handles.video.dataAff.auto=1;
end
initDataAff(hObject,handles);


function gammaSlider_Callback(hObject, eventdata, handles)
handles.video.dataAff.gamma=10^get(hObject,'Value');
initDataAff(hObject,handles);

function gammaSlider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function gamma0_Callback(hObject, eventdata, handles)
handles.video.dataAff.gamma=str2num(get(hObject,'String'));
initDataAff(hObject,handles);

function gamma0_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%  BACKGROUND %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function backpopup_Callback(hObject, eventdata, handles) 
 a=get(hObject,'Value');
 if (isfield(handles.video.background,'imBack'))
    handles.video.background.useBackground=a-1;
 else
    set(hObject,'Value',1);
 end
 handles.video.postOperations.background=handles.video.background.useBackground;
 reloadImage(hObject, handles); 

function background_Callback(hObject, eventdata, handles)
busyOn(handles,'on');
handles.video=handles.video.setBackground();
set(handles.backpopup,'Value',handles.video.postOperations.background+1);
set(handles.backpopup,'enable','on');
set(handles.backfilter,'enable','on');
%set(handles.dispBkg,'enable','on');
reloadImage(hObject,handles);

function backfilter_Callback(hObject, eventdata, handles)
if (isfield(handles.video.background,'imBack'))
    busyOn(handles,'on')
    h = fspecial('average',10);
    handles.video.background.imBack=imfilter(handles.video.background.imBack,h,'replicate');
    reloadImage(hObject,handles);
end

function loadBkg_Callback(hObject, eventdata, handles)
[p,f]=hb_get_file();
[p,f,e]=fileparts(fullfile(p,f));
Im=zeros(2,2);
if (max(strcmpi(e,{'.png','.tif','.jpg','.bmp'}))==1)
    im.cdata=imread(fullfile(p,[f e]));
     if (ndims(im.cdata)==3) 
        Im = (double(rgb2gray(im.cdata)));
     else
        Im = double(im.cdata);
     end
     if (max(max(Im))>2^15) 
               %%REPLIEMENT 16bits
       %        Im(Im>=2^15)=Im(Im>=2^15)-2^16;
       %        Im=2^15+Im;
     end     
elseif (max(strcmpi(e,{'.mat'}))==1)
    Im=importdata(fullfile(p,[f e]));
end
[h,w]=size(Im);
if (h==handles.video.aviInfo.Height && w==handles.video.aviInfo.Width )
    handles.video.background.imBack=Im;
    if (handles.video.postOperations.background==0)
        handles.video.background.useBackground=1;
        handles.video.postOperations.background=1;
    end
    set(handles.backpopup,'enable','on');
    set(handles.backfilter,'enable','on');
    %set(handles.dispBkg,'enable','on');
    
    reloadImage(hObject,handles);
else
    errordlg('Invalid file !')
end


% --- Executes on button press in loadLight.
function loadLight_Callback(hObject, eventdata, handles)
% hObject    handle to loadLight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[p,f]=hb_get_file();
[p,f,e]=fileparts(fullfile(p,f));
Im=zeros(2,2);
if (max(strcmpi(e,{'.png','.tif','.jpg','.bmp'}))==1)
    im.cdata=imread(fullfile(p,[f e]));
     if (ndims(im.cdata)==3) 
        Im = (double(rgb2gray(im.cdata)));
     else
        Im = double(im.cdata);
     end
     if (max(max(Im))>2^15) 
               %%REPLIEMENT 16bits
       %        Im(Im>=2^15)=Im(Im>=2^15)-2^16;
       %        Im=2^15+Im;
     end     
elseif (max(strcmpi(e,{'.mat'}))==1)
    Im=importdata(fullfile(p,[f e]));
end
[h,w]=size(Im);
if (h==handles.video.aviInfo.Height && w==handles.video.aviInfo.Width )
    handles.video.background.eclairage=Im;
    handles.video.background.useBackground=4;
    handles.video.postOperations.background=1;
    
    set(handles.backpopup,'enable','on');
    set(handles.backfilter,'enable','on');
%    set(handles.dispBkg,'enable','on');
    
    reloadImage(hObject,handles);
else
    errordlg('Invalid file !')
end

function calcBkg_Callback(hObject, eventdata, handles)
% reload de l'image sans background
postOperations=handles.video.initPostOperations();
handles.video=handles.video.read(handles.video.currentNbIm,postOperations);
result=hb_calcBkg(handles.video);
if (~isempty(result))
    set(handles.backpopup,'enable','on');
    set(handles.backfilter,'enable','on');
   	handles.video.background.imBack=result;
%    set(handles.dispBkg,'enable','on');
    
    if (handles.video.postOperations.background==0) 
        handles.video.background.useBackground=1;
        handles.video.postOperations.background=1;
    end
     reloadImage(hObject, handles);
end


function showBack_Callback(hObject, eventdata, handles)
if (isfield(handles.video.background,'imBack'))
    imagesc(handles.video.background.imBack);
    set(handles.axes1,'DataAspectRatio',[1 1 1]);
end

   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%  ROI %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ROI_Callback(hObject, eventdata, handles)
handles.video.postOperations.ROI=get(hObject,'Value');
reloadImage(hObject,handles);


function deleteROI_Callback(hObject, eventdata, handles)
handles.video=handles.video.deleteBlackObject();
reloadImage(hObject,handles);

function drawBlack_Callback(hObject, eventdata, handles)
handles.video=handles.video.drawBlackObject('black');
reloadImage(hObject,handles);

function drawWhite_Callback(hObject, eventdata, handles)
handles.video=handles.video.drawBlackObject('white');
reloadImage(hObject,handles);

function invROI_Callback(hObject, eventdata, handles)
if (handles.video.postOperations.crop>0)
    y=handles.video.postOperationsInfos.crop.ly:handles.video.postOperationsInfos.crop.ry;
    x=handles.video.postOperationsInfos.crop.lx:handles.video.postOperationsInfos.crop.rx;
    handles.video.ROI(y,x)=~handles.video.ROI(y,x);
else 
    handles.video.ROI=~handles.video.ROI;
end
reloadImage(hObject,handles);

function autoROI_Callback(hObject, eventdata, handles)
if (handles.video.postOperations.resample>0 || handles.video.postOperations.rotate>0 || handles.video.postOperations.threshold>0) % need to reload
    obj2=handles.video;
    obj2.postOperations.resample=0;
    obj2.postOperations.rotate=0;
    obj2.postOperations.threshold=1;
    obj2=obj2.read(obj2.currentNbIm);
else
    obj2=handles.video.thresholding();
end
if (handles.video.postOperations.crop>0)
    handles.video.ROI(handles.video.postOperationsInfos.crop.ly:handles.video.postOperationsInfos.crop.ry,handles.video.postOperationsInfos.crop.lx:handles.video.postOperationsInfos.crop.rx)=obj2.currentImBin.mainLevel;
else 
    handles.video.ROI=obj2.currentImBin.mainLevel;
end
handles.video.postOperations.ROI=1;
set(handles.ROI,'Value',1);
reloadImage(hObject,handles);

% --- Executes on button press in ShowBlack.
function ShowBlack_Callback(hObject, eventdata, handles)
imagesc(handles.video.ROI);
set(handles.axes1,'DataAspectRatio',[1 1 1]);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% Time 0 %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function SetFinal_Callback(hObject, eventdata, handles)
handles.video.imageTimeZero=handles.video.currentNbIm;
handles.video.time=handles.video.time-handles.video.time(handles.video.currentNbIm);
set(handles.timeImage,'String',num2str(handles.video.time(handles.video.currentNbIm)));
set(handles.start_video,'String',num2str(handles.video.currentNbIm));
guidata(hObject,handles);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%  SAVING  %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function save_Callback(hObject, eventdata, handles)
if (~isvarname(handles.video.filename))
    [pathstr, name, ext] = fileparts(handles.video.filename);
    answer=inputdlg('Name of the workspace variable','Save',1,{name});
    handles.video.filename=answer{1};  
    set(handles.filename,'String',handles.video.filename);
    guidata(hObject,handles);
end
assignin('base', handles.video.filename, handles.video);

function saveAs_Callback(hObject, eventdata, handles)
[p,f,e]=fileparts(handles.video.filename);
def_name=f;
answer=inputdlg('Name of the workspace variable','Save',1,{def_name});
if (length(answer)>0)
    output_var=answer{1};  
    if (isvarname(output_var))
        travVid=handles.video;
        travVid.filename=output_var;
        assignin('base', output_var, travVid);
        guidata(hObject,handles);
    else
        errordlg('Variable name error')
    end
end

function save2file_Callback(hObject, eventdata, handles)
[p,f,e]=fileparts(handles.video.filename);
def_name=f;
[FileName,PathName] = uiputfile('*.mat','Save .mat',def_name);
if (isnumeric(FileName) && FileName==0)
    return
end
[pp,def_name,ext]=fileparts(FileName);
handles.video.filename=def_name;
set(handles.filename,'String',handles.video.filename);
if (isvarname(def_name)==0)
    def_name='video';   
end    
eval([def_name '= handles.video;' ]);
save(fullfile(PathName,FileName),def_name);
guidata(hObject,handles);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%  THRESHOLDING %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% --- Executes on button press in threshold.
function threshold_Callback(hObject, eventdata, handles)
handles.video.postOperations.threshold=get(hObject,'Value');
reloadImage(hObject, handles);


function showHisto_Callback(hObject, eventdata, handles)
handles.video=handles.video.setThreshold();
if (ishandle(handles.threshMenu))
    set(handles.threshMenu,'enable','on');
    set(handles.threshMenu,'Value',2);
    if (handles.video.postOperations.threshold==1)
        reloadImage(hObject, handles);
    else
        guidata(hObject, handles);
    end
end

function showBinIm_Callback(hObject, eventdata, handles)
handles.video=handles.video.read(handles.video.currentNbIm);   
handles.video=handles.video.thresholding();
figure(12)
imagesc(handles.video.currentImBin.mainLevel);
colormap(gray);




function threshMenu_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function threshMenu_Callback(hObject, eventdata, handles)
handles.video.useDataThreshold=get(hObject,'Value')-1;
reloadImage(hObject,handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   EXPORT %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function start_video_Callback(hObject, eventdata, handles)


function start_video_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pas_video_Callback(hObject, eventdata, handles)

function pas_video_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function end_video_Callback(hObject, eventdata, handles)

function end_video_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in record_video.
function record_video_Callback(hObject, eventdata, handles)
im_start=str2num(get(handles.start_video,'String'));
im_end=str2num(get(handles.end_video,'String'));
im_pas=str2num(get(handles.pas_video,'String'));
handles.video.makeFilm(im_start:im_pas:im_end);



% --- Executes on button press in writeImSeq.
function writeImSeq_Callback(hObject, eventdata, handles)
im_start=str2num(get(handles.start_video,'String'));
im_end=str2num(get(handles.end_video,'String'));
im_pas=str2num(get(handles.pas_video,'String'));
handles.video.makeImSeq(im_start:im_pas:im_end);


% --- Executes on button press in writeMatSeq.
function writeMatSeq_Callback(hObject, eventdata, handles)
im_start=str2num(get(handles.start_video,'String'));
im_end=str2num(get(handles.end_video,'String'));
im_pas=str2num(get(handles.pas_video,'String'));
handles.video.makeMatSeq(im_start:im_pas:im_end);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% POST OPERATIONS %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function rotate_Callback(hObject, eventdata, handles)
handles.video.postOperations.rotate=get(hObject,'Value');
reloadImage(hObject,handles);


function angleRotate_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.rotationAngle=str2double(get(hObject,'String'));
if (handles.video.postOperationsInfos.rotationAngle ~= 0) 
    handles.video.postOperations.rotate=1;
    set(handles.rotate,'value',1);
end
reloadImage(hObject,handles);

function angleRotate_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function resampleFactor_Callback(hObject, eventdata, handles)
    busyOn(handles,'on');
   
    factor=str2double(get(hObject,'String'));
    if (factor==1)
        handles.video.postOperations.resample=0;
        handles.video.postOperationsInfos.resample=1;
    else
        handles.video.postOperations.resample=1;
        handles.video.postOperationsInfos.resample=factor;
    end
    
    reloadImage(hObject,handles);
    initAll(handles);
    busyOn(handles,'off');
    

function resampleFactor_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function flipLR_Callback(hObject, eventdata, handles)
handles.video.postOperations.flipLR=get(hObject,'Value');
reloadImage(hObject, handles);


% --- Executes on button press in flipUD.
function flipUD_Callback(hObject, eventdata, handles)
handles.video.postOperations.flipUD=get(hObject,'Value');
reloadImage(hObject, handles);



%%%%%%%%%%%%%%%%%%%%%%
%%%%%% CROP %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%


function cropTLx_Callback(hObject, eventdata, handles)
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles);
    handles=guidata(hObject);
end
gandles.video.postOperations.crop=1;
handles.video.postOperationsInfos.crop.lx=max(min(handles.video.aviInfo.Width,round(str2double(get(hObject,'String')))),1);
cropUpdate(hObject,handles)

function cropTLx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function cropUpdate(hObject,handles)
wi=handles.video.postOperationsInfos.crop.rx-handles.video.postOperationsInfos.crop.lx+1;
he=handles.video.postOperationsInfos.crop.ry-handles.video.postOperationsInfos.crop.ly+1;
factor=1;
if (handles.video.postOperations.resample>0)
    factor=handles.video.postOperationsInfos.resample;
    he=round(he*factor);
    wi=round(wi*factor);
end
set(handles.width,'String',num2str(wi));
set(handles.height,'String',num2str(he));
set(handles.cropBRx,'String',num2str(handles.video.postOperationsInfos.crop.rx));
set(handles.cropBRy,'String',num2str(handles.video.postOperationsInfos.crop.ry));
set(handles.cropTLx,'String',num2str(handles.video.postOperationsInfos.crop.lx));
set(handles.cropTLy,'String',num2str(handles.video.postOperationsInfos.crop.ly));
reloadImage(hObject,handles);

function initCrop(hObject,handles)
handles.video.postOperations.crop=0;
handles.video.postOperationsInfos.crop.rx=handles.video.aviInfo.Width;
handles.video.postOperationsInfos.crop.lx=1;
handles.video.postOperationsInfos.crop.ly=1;
handles.video.postOperationsInfos.crop.ry=handles.video.aviInfo.Height;
guidata(hObject,handles)

function cropBRx_Callback(hObject, eventdata, handles)
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles)    
    handles=guidata(hObject);
end
handles.video.postOperations.crop=1;
handles.video.postOperationsInfos.crop.rx=max(min(handles.video.aviInfo.Width,round(str2double(get(hObject,'String')))),1);
cropUpdate(hObject,handles)



function cropBRx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cropBRy_Callback(hObject, eventdata, handles)
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles);
    handles=guidata(hObject);
end
gandles.video.postOperations.crop=1;
handles.video.postOperationsInfos.crop.ry=max(min(handles.video.aviInfo.Height,round(str2double(get(hObject,'String')))),1);
cropUpdate(hObject,handles)


function cropBRy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cropTLy_Callback(hObject, eventdata, handles)
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles);
    handles=guidata(hObject);
end
handles.video.postOperations.crop=1;
handles.video.postOperationsInfos.crop.ly=max(min(handles.video.aviInfo.Height,round(str2double(get(hObject,'String')))),1);
cropUpdate(hObject,handles)


function cropTLy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ResetCrop_Callback(hObject, eventdata, handles)
initCrop(hObject,handles);
handles=guidata(hObject);
cropUpdate(hObject,handles)



function topLeftButton_Callback(hObject, eventdata, handles)
[x,y]=updateCropButton(hObject,handles);
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles);
    handles=guidata(hObject);
end
handles.video.postOperations.crop=1;
if (handles.video.postOperations.flipUD>0)
    handles.video.postOperationsInfos.crop.ry=max(min(handles.video.aviInfo.Height,round(y)),1);
else
    handles.video.postOperationsInfos.crop.ly=max(min(handles.video.aviInfo.Height,round(y)),1);
end
if (handles.video.postOperations.flipLR>0)
    handles.video.postOperationsInfos.crop.rx=max(min(handles.video.aviInfo.Width,round(x)),1);
else
    handles.video.postOperationsInfos.crop.lx=max(min(handles.video.aviInfo.Width,round(x)),1);
end
cropUpdate(hObject,handles)


function [x,y]=updateCropButton(hObject,handles)
[x,y]=ginput(1);
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles);
    handles=guidata(hObject);
end

factor=1;
if (handles.video.postOperations.resample>0)
    factor=handles.video.postOperationsInfos.resample;
end
[m,n]=size(handles.video.currentIm);
if (handles.video.postOperations.flipUD>0)
    y=m-y+1;
end
if (handles.video.postOperations.flipLR>0)
    x=n-x+1;
end
y=round(y/factor+handles.video.postOperationsInfos.crop.ly-1);
x=round(x/factor+handles.video.postOperationsInfos.crop.lx-1);



function bottomRightButton_Callback(hObject, eventdata, handles)
[x,y]=updateCropButton(hObject,handles);
if (~isfield(handles.video.postOperationsInfos,'crop'))
    initCrop(hObject,handles);
    handles=guidata(hObject);
end
handles.video.postOperations.crop=1;
if (handles.video.postOperations.flipUD>0) 
    handles.video.postOperationsInfos.crop.ly=max(min(handles.video.aviInfo.Height,round(y)),1);
else
    handles.video.postOperationsInfos.crop.ry=max(min(handles.video.aviInfo.Height,round(y)),1);
end
if (handles.video.postOperations.flipLR>0)
    handles.video.postOperationsInfos.crop.lx=max(min(handles.video.aviInfo.Width,round(x)),1);
else
    handles.video.postOperationsInfos.crop.rx=max(min(handles.video.aviInfo.Width,round(x)),1);
end
cropUpdate(hObject,handles)


function filename_Callback(hObject, eventdata, handles)
handles.video.filename=get(hObject,'String');
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function filename_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in setImEnd.
function setImEnd_Callback(hObject, eventdata, handles)
% hObject    handle to setImEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function backpopup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function angleSlider_Callback(hObject, eventdata, handles)
% hObject    handle to angleSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function angleSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angleSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function comment_Callback(hObject, eventdata, handles)
handles.video.comment=get(hObject,'String');
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function comment_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% Time 0 %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function setInitial_Callback(hObject, eventdata, handles)
handles.video.imageTimeZero=handles.video.currentNbIm;
handles.video.time=handles.video.time-handles.video.time(handles.video.currentNbIm);
set(handles.timeImage,'String',num2str(handles.video.time(handles.video.currentNbIm)));
set(handles.start_video,'String',num2str(handles.video.currentNbIm));
guidata(hObject,handles);


% --- Executes on button press in eraseFiles.
function eraseFiles_Callback(hObject, eventdata, handles)
% hObject    handle to eraseFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in affineTransorm.
function affineTransorm_Callback(hObject, eventdata, handles)
% hObject    handle to affineTransorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.video.postOperations.affineTransform
guidata(hObject,handles);
reloadImage(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of affineTransorm



function vecteur_affineTransform_Callback(hObject, eventdata, handles)
% hObject    handle to vecteur_affineTransform (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.video.postOperationsInfos.affineTransform=str2num(get(hObject,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);

% Hints: get(hObject,'String') returns contents of vecteur_affineTransform as text
%        str2double(get(hObject,'String')) returns contents of vecteur_affineTransform as a double


% --- Executes during object creation, after setting all properties.
function vecteur_affineTransform_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vecteur_affineTransform (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in openPartDlg.
function openPartDlg_Callback(hObject, eventdata, handles)
% hObject    handle to openPartDlg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
openPartDetection(handles.video);


% --- Executes on button press in pkfnd.


% --- Executes on button press in bpFilter.
function bpFilter_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.bpass=[str2double(get(handles.bpassValueL,'String')) str2double(get(handles.bpassValueH,'String')) ];
handles.video.postOperations.bpass=get(handles.bpFilter,'Value');
guidata(hObject,handles);
reloadImage(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of bpFilter



function bpassValueL_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.bpass(1)=str2double(get(handles.bpassValueL,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);

function bpassValueH_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.bpass(2)=str2double(get(handles.bpassValueH,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);


% --- Executes during object creation, after setting all properties.
function bpassValueH_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function bpassValueL_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pkfnd_Callback(hObject, eventdata, handles)
handles.video.postOperations.pkfnd=get(handles.pkfnd,'value');
handles.video.postOperationsInfos.pkfnd.Imin=str2double(get(handles.pkfndImin,'String'));
handles.video.postOperationsInfos.pkfnd.size=str2double(get(handles.pkfndSize,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);


function pkfndImin_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.pkfnd.Imin=str2num(get(handles.pkfndImin,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);


% --- Executes during object creation, after setting all properties.
function pkfndImin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pkfndSize_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.pkfnd.size=str2num(get(handles.pkfndSize,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);

function pkfndSize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderPkfndImax_Callback(hObject, eventdata, handles)
 M=max(max(handles.video.currentIm));
 m=min(min(handles.video.currentIm));
v=get(hObject,'value');
handles.video.postOperationsInfos.pkfnd.Imin=m+v*(M-m);
set(handles.pkfndImin,'String',num2str(handles.video.postOperationsInfos.pkfnd.Imin));
guidata(hObject,handles);
reloadImage(hObject, handles);


% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderPkfndImax_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in Trace.
function Trace_Callback(hObject, eventdata, handles)
handles.video.postOperations.trace = get(hObject,'Value');
guidata(hObject,handles);


% --- Executes on button press in Filter.
function Filter_Callback(hObject, eventdata, handles)
handles.video.postOperations.filter=get(hObject,'Value');
handles.video.postOperationsInfos.filter.paramFilter=[str2double(get(handles.ParamFilter1,'String')) str2double(get(handles.ParamFilter2,'String'))];
s=get(handles.FilterType,'String');
handles.video.postOperationsInfos.filter.filterType=s(get(handles.FilterType,'value'),:);
set(handles.ParamFilter1,'enable','on');
if get(handles.FilterType,'value')==2 
    set(handles.ParamFilter2,'enable','on');
else
    set(handles.ParamFilter2,'enable','off');
end
set(handles.FilterType,'enable','on');
guidata(hObject,handles);
reloadImage(hObject, handles);


function ParamFilter1_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.filter.paramFilter=[str2double(get(hObject,'String')) str2double(get(handles.ParamFilter2,'String'))];
guidata(hObject,handles);
reloadImage(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ParamFilter1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ParamFilter2_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.filter.paramFilter=[str2double(get(handles.ParamFilter1,'String')) str2double(get(handles.ParamFilter2,'String'))];
guidata(hObject,handles);
reloadImage(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ParamFilter2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in FilterType.
function FilterType_Callback(hObject, eventdata, handles)
s=get(hObject,'String');
handles.video.postOperationsInfos.filter.filterType=s(get(hObject,'value'),:);
guidata(hObject,handles);
if get(handles.FilterType,'value')==2 
    set(handles.ParamFilter2,'enable','on');
else
    set(handles.ParamFilter2,'enable','off');
end

reloadImage(hObject, handles);


% --- Executes during object creation, after setting all properties.
function FilterType_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in readFPS.
function readFPS_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function readFPS_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function dispPopup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in transpose.
function transpose_Callback(hObject, eventdata, handles)

handles.video.postOperations.transpose=get(hObject,'value');
guidata(hObject,handles);
reloadImage(hObject, handles);


% --- Executes on button press in userDefined_check.
function userDefined_check_Callback(hObject, eventdata, handles)
handles.video.postOperations.userdefined=get(hObject,'Value');
if handles.video.postOperations.userdefined
    if (~isfield (handles.video.postOperationsInfos,'userdefined') || ~exist(func2str(handles.video.postOperationsInfos.userdefined),'file'))
        errordlg('Function not found, please fill the text box first.')
        handles.video.postOperations.userdefined=0;
        set(hObject,'value',0);
    else
        handles.video.postOperations.userdefined=1;
    end
end
guidata(hObject,handles);
reloadImage(hObject, handles);



function userDefined_Callback(hObject, eventdata, handles)
handles.video.postOperationsInfos.userdefined=str2func(get(handles.userDefined,'string'));
guidata(hObject,handles);
reloadImage(hObject, handles);


function userDefined_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ValueR_Callback(hObject, eventdata, handles)
% hObject    handle to ValueR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.video.postOperationsInfos.colors=[0 0 0];
handles.video.postOperationsInfos.colors(1)=str2num(get(handles.ValueR,'String'));
handles.video.postOperationsInfos.colors(2)=str2num(get(handles.ValueV,'String'));
handles.video.postOperationsInfos.colors(3)=str2num(get(handles.ValueB,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);


% Hints: get(hObject,'String') returns contents of ValueR as text
%        str2double(get(hObject,'String')) returns contents of ValueR as a double


% --- Executes during object creation, after setting all properties.
function ValueR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ValueR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ValueV_Callback(hObject, eventdata, handles)
% hObject    handle to ValueV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.video.postOperationsInfos.colors=[0 0 0];
handles.video.postOperationsInfos.colors(1)=str2num(get(handles.ValueR,'String'));
handles.video.postOperationsInfos.colors(2)=str2num(get(handles.ValueV,'String'));
handles.video.postOperationsInfos.colors(3)=str2num(get(handles.ValueB,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);

% Hints: get(hObject,'String') returns contents of ValueV as text
%        str2double(get(hObject,'String')) returns contents of ValueV as a double


% --- Executes during object creation, after setting all properties.
function ValueV_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ValueV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ValueB_Callback(hObject, eventdata, handles)
% hObject    handle to ValueB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.video.postOperationsInfos.colors=[0 0 0];
handles.video.postOperationsInfos.colors(1)=str2num(get(handles.ValueR,'String'));
handles.video.postOperationsInfos.colors(2)=str2num(get(handles.ValueV,'String'));
handles.video.postOperationsInfos.colors(3)=str2num(get(handles.ValueB,'String'));
guidata(hObject,handles);
reloadImage(hObject, handles);
% Hints: get(hObject,'String') returns contents of ValueB as text
%        str2double(get(hObject,'String')) returns contents of ValueB as a double


% --- Executes during object creation, after setting all properties.
function ValueB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ValueB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
