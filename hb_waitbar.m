function h=hb_waitbar(h,x)

if nargin==0
    tic;
    h=waitbar(0,'Please wait...');
else
     tElapsed = toc;

end
try
    waitbar(x,h,sprintf('%g s remaining',round((1-x)./x*tElapsed)));
end

       