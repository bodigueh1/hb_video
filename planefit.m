function [p,imfit]=planefit(im,n,ROI,x,y) 
% PLANEFIT. polynomial fitting of a 2D matrix
%
% SYNTAX 
%
%   p=planefit(im)  
%   p=planefit(im,n)
%   p=planefit(im,n,ROI)
%   p=planefit(im,n,ROI)
%   [p,imfit]=planefit(im,...)
%
% DESCRIPTION
%   
%   p=planefit(im) returns the coefficients of the 2d plane that fits
%   the 2d matrix im of size M*N. The results p is a 2*2 matrix so that the fitted plane
%   is 
%       P(x,y) = p22*x*y+ p12*x + p21*y +p11 
%   where x and y are the normalized subscripts [j,i] of im : x= i/N and y= j/N
%  
%   p=planefit(im,order) returns the coefficients of the polynomial
%   function of degree n that fits the 2d image im. The results p is a
%   n+1*n+1 matrix that contains the coefficients of the polynomial
%   function in descending powers :
%       P(x,y) = SUM(pij*x^(i)*y^(j))
%   
%   p=planefit(im,order,ROI) computes the best polynomial fit using a region
%   of interest specified by the logical M*N matrix ROI
%   
%   p=planefit(im,order,ROI,x,y) uses the speciefied x and y 1D vectors of
%   length N and M instead of the normalized subscprits of im
%   
%   [P,imfit]=planefit(image, ...) also returns the image od the fit that
%   has the same dimension of the original image
%


all=true(size(im));
if (nargin==1)
    n=1;
end
if (nargin<=2)
    ROI=all;
end
if (nargout<2)
    all=ROI;
end

[M,N]=size(im);
NR=sum(sum(all));
x=ones(M,1)*[1:N]/N;
y=[1:M]'*ones(1,N)/M;
vecx=(x(all)*ones(1,n+1)).^(ones(NR,1)*[0:n]);
vecy=y(all)*ones(1,n+1);
vec=zeros(NR,(n+1).^2);
clear x y;
for i=0:n
    vec(:,i*(n+1)+1:(i+1)*(n+1))=vecx.*vecy.^i;
end
clear vecx vecy;

% do the fit 
if (nargout~=2)
    res=vec\im(ROI);
else
    res=vec(ROI,:)\im(ROI);
end

p=reshape(res,n+1,n+1);

if nargout==2
    imfit=reshape(vec*res,M,N);
end