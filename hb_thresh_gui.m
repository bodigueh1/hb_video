function varargout = hb_thresh_gui(varargin)
% HB_THRESH_GUI M-file for hb_thresh_gui.fig
%      HB_THRESH_GUI, by itself, creates a new HB_THRESH_GUI or raises the existing
%      singleton*.
%
%      H = HB_THRESH_GUI returns the handle to a new HB_THRESH_GUI or the handle to
%      the existing singleton*.
%
%      HB_THRESH_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HB_THRESH_GUI.M with the given input arguments.
%
%      HB_THRESH_GUI('Property','Value',...) creates a new HB_THRESH_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before hb_thresh_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to hb_thresh_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help hb_thresh_gui

% Last Modified by GUIDE v2.5 10-Oct-2008 10:46:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hb_thresh_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @hb_thresh_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before hb_thresh_gui is made visible.
function hb_thresh_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to hb_thresh_gui (see VARARGIN)

% Choose default command line output for hb_thresh_gui
handles.output = hObject;

handles.im=varargin{1};     
if (length(varargin)==2) 
    handles.ROI=varargin{2};
else
    [m,n]=size(handles.im);
    handles.ROI=logical(ones(m,n));
end

set(gcf,'CurrentAxes',handles.axes1);
imagesc(handles.im);
axis off;
set(handles.axes1,'DataAspectRatio',[1 1 1]);
colormap(gray);
zoom reset;   


handles.datas.minImage=min(min(handles.im(handles.ROI)));
handles.im(handles.ROI)=handles.im(handles.ROI)-handles.datas.minImage;
handles.datas.maxImage=max(max(handles.im(handles.ROI)));
handles.im(handles.ROI)=handles.im(handles.ROI)/handles.datas.maxImage;
handles.datas.mainLevel=graythresh(handles.im(handles.ROI));
handles.datas.currentLevel=handles.datas.mainLevel;
handles.datas.upperLevel=handles.datas.mainLevel*1.05;
handles.datas.lowerLevel=handles.datas.mainLevel*0.95;
handles.datas.currentProperty='mainLevel';
handles.datas.useInterval=(get(handles.useInterval,'Value')==1);
handles.datas.ok=0;
lin=reshape(handles.im(handles.ROI),1,[]);
[h,x]=hist(double(lin),linspace(min(lin), max(lin),255));
handles.datas.x_histo=x;
handles.datas.h=h;
guidata(hObject,handles);
do_update_thresh(hObject,handles);
uiwait;

function varargout = hb_thresh_gui_OutputFcn(hObject, eventdata, handles) 
%hh=guidata(handles.validate);
%assignin('base',hh,handles);
if (~isfield(handles,'datas'))
    varargout{1} = [];
else
     varargout{1} = handles.datas;
     close
end




function level_Callback(hObject, eventdata, handles)
handles.datas.currentLevel=str2num(get(hObject,'String'));
do_update_thresh(hObject,handles);


function level_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function slider1_Callback(hObject, eventdata, handles)
handles.datas.currentLevel=get(hObject,'Value');
do_update_thresh(hObject,handles);

function slider1_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function Validate_Callback(hObject, eventdata, handles)
handles.datas.ok=1;
guidata(hObject,handles);
uiresume;



function lowerLevel_Callback(hObject, eventdata, handles)
handles.datas.currentProperty='lowerLevel';
handles.datas.currentLevel=get(hObject,'String');
do_update_thresh(hObject,handles);

function lowerLevel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function mainLevel_Callback(hObject, eventdata, handles)
handles.datas.currentProperty='mainLevel';
handles.datas.currentLevel=get(hObject,'String');
do_update_thresh(hObject,handles);

function mainLevel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function upperLevel_Callback(hObject, eventdata, handles)
handles.datas.currentProperty='upperLevel';
handles.datas.currentLevel=get(hObject,'String');
do_update_thresh(hObject,handles);

function upperLevel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function radioLowerBond_Callback(hObject, eventdata, handles)
handles.datas.currentProperty='lowerLevel';
handles.datas.currentLevel=handles.datas.lowerLevel;
do_update_thresh(hObject,handles);

function radioMainLevel_Callback(hObject, eventdata, handles)
handles.datas.currentProperty='mainLevel';
handles.datas.currentLevel=handles.datas.mainLevel;
do_update_thresh(hObject,handles);

function radioUpperBond_Callback(hObject, eventdata, handles)
handles.datas.currentProperty='upperLevel';
handles.datas.currentLevel=handles.datas.upperLevel;
do_update_thresh(hObject,handles);

function useInterval_Callback(hObject, eventdata, handles)
if (get(hObject,'Value')==1)
    vis='on';
else
    vis='off';
    handles.datas.currentLevel=handles.datas.mainLevel;
    handles.datas.currentProperty='mainLevel';
end
handles.datas.useInterval=(get(hObject,'Value')==1);
set(handles.lowerLevel,'visible',vis);
set(handles.upperLevel,'visible',vis);
set(handles.radioUpperBond,'visible',vis);
set(handles.radioLowerBond,'visible',vis);
set(handles.calInterval,'visible',vis);
do_update_thresh(hObject,handles);

function calInterval_Callback(hObject, eventdata, handles)
handles.datas.currentLevel=handles.datas.mainLevel;
handles.datas.currentProperty='mainLevel';
handles.datas.lowerLevel=handles.datas.currentLevel*0.95;
handles.datas.upperLevel=handles.datas.currentLevel*1.05;
do_update_thresh(hObject,handles);


function autoLevel_Callback(hObject, eventdata, handles)
handles.datas.currentLevel=graythresh(handles.im(handles.ROI));
do_update_thresh(hObject,handles);


function do_update_thresh(hObject,handles)

set(handles.radioMainLevel,'Value',0);
set(handles.radioUpperBond,'Value',0);
set(handles.radioLowerBond,'Value',0);
switch handles.datas.currentProperty
    case 'mainLevel'
        set(handles.radioMainLevel,'Value',1);
        handles.datas.mainLevel=handles.datas.currentLevel;
    case 'upperLevel'
        set(handles.radioUpperBond,'Value',1);
        handles.datas.upperLevel=handles.datas.currentLevel;
    case 'lowerLevel'
        set(handles.radioLowerBond,'Value',1);
        handles.datas.lowerLevel=handles.datas.currentLevel;
end

 set(handles.lowerLevel,'String',num2str(handles.datas.lowerLevel));
 set(handles.upperLevel,'String',num2str(handles.datas.upperLevel));
 set(handles.mainLevel,'String',num2str(handles.datas.mainLevel));
 set(handles.slider1,'Value',handles.datas.currentLevel);
 set(handles.level,'String',num2str(handles.datas.currentLevel));

 im=handles.im;
if (isfield(handles.datas,'postFilter') && handles.datas.postFilter>0)
    h = fspecial('disk',handles.datas.postFilter);
   im=imfilter(im,h,'replicate');
end
 
 
imBW=im2bw(im,handles.datas.currentLevel);
%if (isfield(handles.datas,'postFilter') && handles.datas.postFilter>0)
   % h = fspecial('gaussian',handles.datas.postFilter);
   %imBW=logical(imfilter(imBW,h,'replicate'));
%end
set(gcf,'CurrentAxes',handles.axes2); 
linkaxes([handles.axes1 handles.axes2],'off');
imagesc(imBW);
set(handles.axes2,'DataAspectRatio',[1 1 1]);
colormap(gray);
axis off;
linkaxes([handles.axes1 handles.axes2],'xy');
set(gcf,'CurrentAxes',handles.axes3);
bar(handles.datas.x_histo,handles.datas.h, 'b');
hold on;
bar([handles.datas.currentLevel-0.005 handles.datas.currentLevel handles.datas.currentLevel+0.005 ],[0 max(handles.datas.h) 0],'r');
hold off;
axis([0 1 0 max(handles.datas.h)]);    
guidata(hObject, handles);


% --------------------------------------------------------------------
function zoomIn_ClickedCallback(hObject, eventdata, handles)
pan off; 
h=zoom(gcf)
set(h,'enable','on');
set(h,'direction','in');


% --------------------------------------------------------------------
function zoomOut_ClickedCallback(hObject, eventdata, handles)
pan off; 
h=zoom(gcf)
set(h,'enable','on');
set(h,'direction','out');




% --------------------------------------------------------------------
function panFig_OffCallback(hObject, eventdata, handles)
zoom off;
 pan off;

% --------------------------------------------------------------------
function panFig_OnCallback(hObject, eventdata, handles)
zoom off;
 pan on;


% --------------------------------------------------------------------
function zoomOut_OffCallback(hObject, eventdata, handles)
pan off;
zoom off;
% --------------------------------------------------------------------
function zoomOut_OnCallback(hObject, eventdata, handles)
pan off; 
h=zoom(gcf);
set(h,'enable','on');
set(h,'direction','out');

% --------------------------------------------------------------------
	
pan off;
zoom off;

% --------------------------------------------------------------------
function zoomIn_OnCallback(hObject, eventdata, handles)
pan off; 
h=zoom(gcf);
set(h,'enable','on');
set(h,'direction','in');


% --- Executes on button press in postFilter.
function postFilter_Callback(hObject, eventdata, handles)
if (get(hObject,'Value')==1)
    handles.datas.postFilter=str2num(get(handles.postFilterSize,'String'));
else
    handles.datas.postFilter=0;
end
guidata(hObject, handles);  
do_update_thresh(hObject,handles);

function postFilterSize_Callback(hObject, eventdata, handles)
if (get(handles.postFilter,'Value')==1)
    handles.datas.postFilter=str2num(get(handles.postFilterSize,'String'));
end
guidata(hObject, handles);  
do_update_thresh(hObject,handles);


% --- Executes during object creation, after setting all properties.
function postFilterSize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


